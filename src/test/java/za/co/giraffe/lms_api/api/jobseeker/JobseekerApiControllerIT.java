package za.co.giraffe.lms_api.api.jobseeker;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import za.co.giraffe.lms_api.config.ControllerTestConfig;
import za.co.giraffe.lms_api.model.jobseeker.EnrollCourseResponse;
import za.co.giraffe.lms_api.model.request.UpdateCourseStatusRequest;
import za.co.giraffe.lms_api.utils.HttpUtils;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

import static org.assertj.core.api.Assertions.assertThat;

public class JobseekerApiControllerIT extends ControllerTestConfig {

    @Test
    @Order(21)
    public void enrollCourse() throws JsonProcessingException {
        makeHttpRequest(HttpMethod.POST, "/admin/category", HttpUtils.createLmsCategoryRequest());
        makeHttpRequest(HttpMethod.POST, "/admin/course", HttpUtils.createLmsCourseRequest());
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.POST, "/jobseekers/1/course/1/action/enroll", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(22)
    public void getEnrolledCourse() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/jobseekers/1/course", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(23)
    public void checkIfJobseekerHasEnrolledThisCourse() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/jobseekers/1/course/1", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(24)
    public void enrollCourseBadRequest() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.POST, "/jobseekers/1/course/1/action/enroll", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    @Order(25)
    public void upsertCourseChapterStatus() throws IOException {
        Integer enrollmentId = commonEnrollCourse();
        UpdateCourseStatusRequest updateCourseStatusRequest = new UpdateCourseStatusRequest();
        updateCourseStatusRequest.setCompletedChapter(2);
        updateCourseStatusRequest.setCurrentChapter(1);
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.PATCH, "/jobseekers/2/enrollment/" + enrollmentId + "/action/update-chapter-status", updateCourseStatusRequest);
        System.out.println(responseEntity.getBody());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(26)
    public void fetchCourseChapterStatus() throws IOException {
        Integer enrollmentId = commonEnrollCourse();
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/jobseekers/3/enrollment/" + enrollmentId + "/status", null);
        System.out.println(responseEntity.getBody());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }


    private Integer commonEnrollCourse() throws IOException {
        makeHttpRequest(HttpMethod.POST, "/admin/category", HttpUtils.createLmsCategoryRequest());
        makeHttpRequest(HttpMethod.POST, "/admin/course", HttpUtils.createLmsCourseRequest());
        int randomWithNextInt = ThreadLocalRandom.current().nextInt(11, 20);
        ;
        ResponseEntity<String> responseEntity1 = makeHttpRequest(HttpMethod.POST, "/jobseekers/" + randomWithNextInt + "/course/1/action/enroll", null);
        System.out.println(responseEntity1.getBody());
        EnrollCourseResponse enrollCourseResponse = objectMapper.readValue(responseEntity1.getBody(), EnrollCourseResponse.class);
        return enrollCourseResponse.getEnrollmentId();
    }
}
