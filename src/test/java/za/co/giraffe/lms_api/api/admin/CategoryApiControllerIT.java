package za.co.giraffe.lms_api.api.admin;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import za.co.giraffe.lms_api.config.ControllerTestConfig;
import za.co.giraffe.lms_api.model.request.LmsCategoryRequest;
import za.co.giraffe.lms_api.model.request.PublishRequest;
import za.co.giraffe.lms_api.utils.HttpUtils;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Suraj Tekchandani
 */
public class CategoryApiControllerIT extends ControllerTestConfig {

    @Test
    @Order(2)
    public void getCategories() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/admin/category", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(1)
    public void createCategory() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.POST, "/admin/category", HttpUtils.createLmsCategoryRequest());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(3)
    public void updateCategory() throws JsonProcessingException {
        LmsCategoryRequest lmsCategoryRequest = new LmsCategoryRequest();
        lmsCategoryRequest.setName("test updated");
        lmsCategoryRequest.setPublished(true);
        lmsCategoryRequest.setFileType("png");
        lmsCategoryRequest.setLogo(HttpUtils.getImageBytes());
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.PUT, "/admin/category/1", lmsCategoryRequest);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(4)
    public void updateCategoryBadRequest() throws JsonProcessingException {
        LmsCategoryRequest lmsCategoryRequest = new LmsCategoryRequest();
        lmsCategoryRequest.setName("test updated");
        lmsCategoryRequest.setPublished(true);
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.PUT, "/admin/category/144", lmsCategoryRequest);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }


    @Test
    @Order(5)
    public void publishCategoriesById() throws JsonProcessingException {
        PublishRequest publishRequest = new PublishRequest();
        publishRequest.setPublished(true);
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.PATCH, "/admin/category/1", publishRequest);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    @Order(6)
    public void getCategoriesById() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/admin/category/1", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(7)
    public void getCategoriesByIdBadRequest() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/admin/category/144", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }
}
