package za.co.giraffe.lms_api.api.jobseeker;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import za.co.giraffe.lms_api.config.ControllerTestConfig;
import za.co.giraffe.lms_api.model.request.CourseReviewRequest;
import za.co.giraffe.lms_api.utils.HttpUtils;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Suraj Tekchandani
 */
public class PublicApiControllerIT extends ControllerTestConfig {

    @Test
    @Order(14)
    public void getJobseekerCategories() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/public/category", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(15)
    public void getJobseekerCourse() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/public/course", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(16)
    public void getJobseekerCourseById() throws JsonProcessingException {
        makeHttpRequest(HttpMethod.POST, "/admin/category", HttpUtils.createLmsCategoryRequest());
        makeHttpRequest(HttpMethod.POST, "/admin/course", HttpUtils.createLmsCourseRequest());
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/public/course/1", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(17)
    public void reviewCourse() throws JsonProcessingException {
        CourseReviewRequest courseReviewRequest = new CourseReviewRequest();
        courseReviewRequest.setEmail("test@gmail.com");
        courseReviewRequest.setRating(5);
        courseReviewRequest.setReview("test review");
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.POST, "/public/course/1/action/review", courseReviewRequest);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(18)
    public void healthCheck() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/public/health-check", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(19)
    public void updateCourseReview() throws JsonProcessingException {
        CourseReviewRequest courseReviewRequest = new CourseReviewRequest();
        courseReviewRequest.setEmail("test@gmail.com");
        courseReviewRequest.setRating(10);
        courseReviewRequest.setReview("test review");
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.POST, "/public/course/1/action/review", courseReviewRequest);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(20)
    public void getJobseekerCourseByIdBadRequest() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/public/course/15555", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    @Order(32)
    public void testSagger() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}
