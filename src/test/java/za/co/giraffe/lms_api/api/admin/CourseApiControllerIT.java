package za.co.giraffe.lms_api.api.admin;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import za.co.giraffe.lms_api.config.ControllerTestConfig;
import za.co.giraffe.lms_api.model.lms.CategoryResponse;
import za.co.giraffe.lms_api.model.lms.CourseResponse;
import za.co.giraffe.lms_api.model.request.CourseChapterRequest;
import za.co.giraffe.lms_api.model.request.CourseRequest;
import za.co.giraffe.lms_api.model.request.PublishRequest;
import za.co.giraffe.lms_api.utils.HttpUtils;

import java.io.IOException;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Suraj Tekchandani
 */
public class CourseApiControllerIT extends ControllerTestConfig {

    @Test
    @Order(8)
    public void getCourses() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/admin/course", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(9)
    public void getCoursesByIdBadRequest() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/admin/course/144", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    @Order(10)
    public void createCourses() throws JsonProcessingException {
        makeHttpRequest(HttpMethod.POST, "/admin/category", HttpUtils.createLmsCategoryRequest());
        CourseRequest courseRequest = HttpUtils.createLmsCourseRequest();
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.POST, "/admin/course", courseRequest);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(11)
    public void publishCoursesById() throws JsonProcessingException {
        PublishRequest publishRequest = new PublishRequest();
        publishRequest.setPublished(true);
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.PATCH, "/admin/course/1", publishRequest);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    @Order(12)
    public void getCoursesById() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/admin/course/1", null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(13)
    public void updateCourses() throws JsonProcessingException {
        CourseRequest courseRequest = new CourseRequest();
        courseRequest.setCategoryId(1);
        courseRequest.setName("test111");
        courseRequest.setOverview("test tes22t");
        courseRequest.setPublished(true);
        courseRequest.setFileType("png");
        courseRequest.setFile(HttpUtils.getImageBytes());
        CourseChapterRequest courseChapterRequest = new CourseChapterRequest();
        courseChapterRequest.setChronology((short) 0);
        courseChapterRequest.setContent("test");
        courseChapterRequest.setName("test");
        CourseChapterRequest courseChapterRequest2 = new CourseChapterRequest();
        courseChapterRequest2.setChronology((short) 1);
        courseChapterRequest2.setContent("test");
        courseChapterRequest2.setName("test");
        courseChapterRequest2.setId(2);
        courseRequest.setDeletedChapters(Arrays.asList(1));
        courseRequest.setChapters(Arrays.asList(courseChapterRequest, courseChapterRequest2));
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.PUT, "/admin/course/1", courseRequest);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }


    @Test
    @Order(29)
    public void updateCoursesBadRequest() throws JsonProcessingException {
        CourseRequest courseRequest = new CourseRequest();
        courseRequest.setCategoryId(1);
        courseRequest.setName("test111");
        courseRequest.setOverview("test tes22t");
        courseRequest.setPublished(true);
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.PUT, "/admin/course/1555", courseRequest);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    @Order(30)
    public void updateCoursesWithCategory() throws IOException {
        ResponseEntity<String> responseEntityCategory = makeHttpRequest(HttpMethod.POST, "/admin/category", HttpUtils.createLmsCategoryRequest());
        CategoryResponse categoryResponse = objectMapper.readValue(responseEntityCategory.getBody(), CategoryResponse.class);
        CourseRequest courseRequest = new CourseRequest();
        courseRequest.setCategoryId(categoryResponse.getId());
        courseRequest.setName("test111");
        courseRequest.setOverview("test tes22t");
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.PUT, "/admin/course/1", courseRequest);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }


    @Test
    @Order(31)
    public void updateCoursesAddImage() throws IOException {
        CourseRequest courseRequest = new CourseRequest();
        courseRequest.setCategoryId(1);
        courseRequest.setName("test111");
        courseRequest.setOverview("test tes22t");
        ResponseEntity<String> responseEntityCourse = makeHttpRequest(HttpMethod.POST, "/admin/course", courseRequest);
        CourseResponse courseResponse = objectMapper.readValue(responseEntityCourse.getBody(), CourseResponse.class);
        CourseRequest updateCourseRequest = new CourseRequest();
        updateCourseRequest.setCategoryId(1);
        updateCourseRequest.setName("test11111");
        updateCourseRequest.setFileType("png");
        updateCourseRequest.setFile(HttpUtils.getImageBytes());
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.PUT, "/admin/course/" + courseResponse.getId(), updateCourseRequest);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

}
