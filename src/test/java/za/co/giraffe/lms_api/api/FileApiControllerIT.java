package za.co.giraffe.lms_api.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import za.co.giraffe.lms_api.config.ControllerTestConfig;
import za.co.giraffe.lms_api.model.request.FileUploadRequest;
import za.co.giraffe.lms_api.utils.HttpUtils;

import java.io.IOException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class FileApiControllerIT extends ControllerTestConfig {

    @Test
    @Order(27)
    public void uploadLmsFile() throws IOException {
        FileUploadRequest fileUploadRequest = new FileUploadRequest();
        fileUploadRequest.setFileType("png");
        fileUploadRequest.setFile(HttpUtils.getImageBytes());
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.POST, "/file/upload", fileUploadRequest);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        String fileId = responseEntity.getBody().replace("http://localhost:8081/file/", "");
        ResponseEntity<String> responseEntityGetFile = makeHttpRequest(HttpMethod.GET, "/file/" + fileId, null);
        assertThat(responseEntityGetFile.getStatusCode()).isEqualTo(HttpStatus.OK);
    }


    @Test
    @Order(28)
    public void getLmsFile() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.GET, "/file/" + UUID.randomUUID().toString(), null);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    @Order(33)
    public void uploadLmsFileWithOutFileType() throws IOException {
        FileUploadRequest fileUploadRequest = new FileUploadRequest();
        fileUploadRequest.setFile(HttpUtils.getImageBytes());
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.POST, "/file/upload", fileUploadRequest);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    @Order(34)
    public void uploadLmsFileWithOutFile() throws IOException {
        FileUploadRequest fileUploadRequest = new FileUploadRequest();
        fileUploadRequest.setFileType("png");
        ResponseEntity<String> responseEntity = makeHttpRequest(HttpMethod.POST, "/file/upload", fileUploadRequest);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
