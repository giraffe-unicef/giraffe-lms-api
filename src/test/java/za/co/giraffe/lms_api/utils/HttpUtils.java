package za.co.giraffe.lms_api.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import za.co.giraffe.lms_api.model.request.CourseChapterRequest;
import za.co.giraffe.lms_api.model.request.CourseRequest;
import za.co.giraffe.lms_api.model.request.LmsCategoryRequest;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/**
 * @author Suraj Tekchandani
 */
public class HttpUtils {

    public static String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }

    public static String createUrl(String uri, int port) {
        return "http://localhost:" + port + uri;
    }

    public static byte[] getImageBytes() {
        try {
            ClassPathResource res = new ClassPathResource("test/boost.png");
            InputStream is = res.getInputStream();
            return IOUtils.toByteArray(is);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static LmsCategoryRequest createLmsCategoryRequest() {
        LmsCategoryRequest lmsCategoryRequest = new LmsCategoryRequest();
        lmsCategoryRequest.setColor("#e2e2e2e");
        lmsCategoryRequest.setDescription("test description");
        lmsCategoryRequest.setName("test");
        lmsCategoryRequest.setPublished(true);
        lmsCategoryRequest.setFileType("png");
        lmsCategoryRequest.setLogo(HttpUtils.getImageBytes());
        return lmsCategoryRequest;
    }

    public static CourseRequest createLmsCourseRequest() {
        CourseRequest courseRequest = new CourseRequest();
        courseRequest.setCategoryId(1);
        courseRequest.setName("test");
        courseRequest.setOverview("test test");
        courseRequest.setPublished(true);
        courseRequest.setFileType("png");
        courseRequest.setFile(HttpUtils.getImageBytes());
        CourseChapterRequest courseChapterRequest = new CourseChapterRequest();
        courseChapterRequest.setChronology((short) 0);
        courseChapterRequest.setContent("test");
        courseChapterRequest.setName("test");
        CourseChapterRequest courseChapterRequest1 = new CourseChapterRequest();
        courseChapterRequest1.setChronology((short) 1);
        courseChapterRequest1.setContent("test1");
        courseChapterRequest1.setName("test1");
        CourseChapterRequest courseChapterRequest2 = new CourseChapterRequest();
        courseChapterRequest2.setChronology((short) 2);
        courseChapterRequest2.setContent("test2");
        courseChapterRequest2.setName("test3");
        courseRequest.setChapters(Arrays.asList(courseChapterRequest, courseChapterRequest1, courseChapterRequest2));
        return courseRequest;
    }
}
