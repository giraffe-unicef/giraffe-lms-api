package za.co.giraffe.lms_api.config;

import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import za.co.giraffe.lms_api.utils.HttpUtils;

import javax.annotation.PostConstruct;

/**
 * @author Suraj Tekchandani
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public abstract class ControllerTestConfig {
    @LocalServerPort
    protected int port;
    protected TestRestTemplate restTemplate = new TestRestTemplate();
    protected HttpHeaders headers = new HttpHeaders();
    protected ObjectMapper objectMapper = new ObjectMapper();


    @BeforeClass
    public static void setUpClass() {
        FixtureFactoryLoader.loadTemplates("br.com.six2six.template");
    }

    @PostConstruct
    public void setUp() {
        objectMapper.registerModule(new JavaTimeModule());
    }


    @Before
    public void setUpTest() {
    }


    public ResponseEntity<String> makeHttpRequest(HttpMethod httpMethod, String url, Object body) throws JsonProcessingException {
        HttpEntity<String> entity;
        if (!httpMethod.equals(HttpMethod.GET)) {
            headers.setContentType(MediaType.APPLICATION_JSON);
            entity = new HttpEntity<>(body instanceof String ? String.valueOf(body) : HttpUtils.mapToJson(body), headers);
        } else {
            entity = new HttpEntity<>(null, headers);
        }

        ResponseEntity<String> response = restTemplate.exchange(
                HttpUtils.createUrl(url, port), httpMethod, entity, String.class);
        return response;
    }
}
