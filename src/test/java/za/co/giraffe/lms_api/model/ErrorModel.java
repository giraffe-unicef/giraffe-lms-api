package za.co.giraffe.lms_api.model;

import org.junit.jupiter.api.Test;
import za.co.giraffe.lms_api.model.error.ApiValidationError;

import static org.junit.Assert.assertEquals;

public class ErrorModel {

    @Test
    public void ApiValidationErrorModelTest() {
        ApiValidationError error = new ApiValidationError("test", "test", "test", "this is test error");
        assertEquals(error.getField(), "test");
        assertEquals(error.getObject(), "test");
        assertEquals(error.getRejectedValue(), "test");
        assertEquals(error.getMessage(), "this is test error");

    }


}
