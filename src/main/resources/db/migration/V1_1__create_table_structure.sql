-- ----------------------------
-- Schema  learning_management_system
-- ----------------------------
CREATE SCHEMA learning_management_system;
-- ----------------------------
-- Table structure for file_upload
-- ----------------------------
CREATE TABLE "learning_management_system"."file_upload" (
  "file_id" uuid NOT NULL,
  "file_path" varchar(500) COLLATE "pg_catalog"."default" NOT NULL,
  "created_date" timestamptz(6),
  "file_type" varchar(255) COLLATE "pg_catalog"."default",
  CONSTRAINT "file_upload_pkey" PRIMARY KEY ("file_id")
)
;

-- ----------------------------
-- Table structure for category
-- ----------------------------
CREATE TABLE "learning_management_system"."category" (
  "id" serial4,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "description" text COLLATE "pg_catalog"."default",
  "file_id" uuid,
  "color" varchar(255) COLLATE "pg_catalog"."default",
  "published" bool,
  "created_date" timestamptz(6),
  "published_date" timestamptz(6),
  CONSTRAINT "category_pkey" PRIMARY KEY ("id")
)
;


ALTER TABLE "learning_management_system"."category" ADD CONSTRAINT "file_upload" FOREIGN KEY ("file_id") REFERENCES "learning_management_system"."file_upload" ("file_id") ON DELETE NO ACTION ON UPDATE NO ACTION;


-- ----------------------------
-- Table structure for course
-- ----------------------------
CREATE TABLE "learning_management_system"."course" (
  "id" serial4,
  "name" varchar(500) COLLATE "pg_catalog"."default",
  "overview" text COLLATE "pg_catalog"."default",
  "published" bool,
  "created_date" timestamptz(6),
  "published_date" timestamptz(6),
  "category_id" int4,
  "file_id" uuid,
  "time_to_read" int4,
  CONSTRAINT "course_pkey" PRIMARY KEY ("id")
)
;


ALTER TABLE "learning_management_system"."course" ADD CONSTRAINT "category_associated" FOREIGN KEY ("category_id") REFERENCES "learning_management_system"."category" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "learning_management_system"."course" ADD CONSTRAINT "file_associated" FOREIGN KEY ("file_id") REFERENCES "learning_management_system"."file_upload" ("file_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Table structure for course_chapter
-- ----------------------------
CREATE TABLE "learning_management_system"."course_chapter" (
  "id" serial4,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "content" text COLLATE "pg_catalog"."default",
  "chronology" int2,
  "course_id" int4,
  CONSTRAINT "course_chapter_pkey" PRIMARY KEY ("id")
)
;


ALTER TABLE "learning_management_system"."course_chapter" ADD CONSTRAINT "chapter_course" FOREIGN KEY ("course_id") REFERENCES "learning_management_system"."course" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Table structure for jobseeker_course
-- ----------------------------
DROP TABLE IF EXISTS "learning_management_system"."jobseeker_course";
CREATE TABLE "learning_management_system"."jobseeker_course" (
  "id" serial4,
  "jobseeker_id" int4,
  "course_id" int4,
  "current_chapter_chronology" int4,
  "max_completed_chapter_chronology" int4,
  "total_chapter" int4,
  "enrollment_date" timestamptz(6),
  "last_updated_date" timestamptz(6)
)
;

-- ----------------------------
-- Primary Key structure for table jobseeker_course
-- ----------------------------
ALTER TABLE "learning_management_system"."jobseeker_course" ADD CONSTRAINT "jobseeker_course_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table jobseeker_course
-- ----------------------------
ALTER TABLE "learning_management_system"."jobseeker_course" ADD CONSTRAINT "jobseeker_course_course_id_fk" FOREIGN KEY ("course_id") REFERENCES "learning_management_system"."course" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;


-- ----------------------------
-- Table structure for course_review
-- ----------------------------

CREATE TABLE "learning_management_system"."course_review" (
  "id" serial4,
  "course_id" int4,
  "like" bool,
  "review" text COLLATE "pg_catalog"."default",
  "created_date" timestamptz(6),
  "rating" int4,
  "email" varchar(255) COLLATE "pg_catalog"."default",
  CONSTRAINT "course_review_pkey" PRIMARY KEY ("id")
)
;

--ALTER TABLE "learning_management_system"."course_review" ADD CONSTRAINT "course_review_jobseeker_course_id_fk" FOREIGN KEY ("jobseeker_course_id") REFERENCES "learning_management_system"."jobseeker_course" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- COMMENT ON CONSTRAINT "course_review_jobseeker_course_id_uk" ON "learning_management_system"."course_review" IS ''abvoid multiple reviews for same enrollment'';