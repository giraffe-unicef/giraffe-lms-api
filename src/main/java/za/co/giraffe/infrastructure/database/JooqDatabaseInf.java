package za.co.giraffe.infrastructure.database;

import org.jooq.DSLContext;

/**
 * Created by drunkenbear on 2016/08/12.
 */
public interface JooqDatabaseInf extends DatabaseInf {
    void withTransaction(DSLContext context);
}
