package za.co.giraffe.infrastructure.database;

/**
 * Created by drunkenbear on 2016/08/12.
 */

import java.sql.Connection;
import java.sql.SQLException;

public interface DatabaseInf {
    Connection getConnection() throws SQLException;
}
