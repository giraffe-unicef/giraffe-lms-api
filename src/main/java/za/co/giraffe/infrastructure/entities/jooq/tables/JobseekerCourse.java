/*
 * This file is generated by jOOQ.
 */
package za.co.giraffe.infrastructure.entities.jooq.tables;


import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;

import za.co.giraffe.infrastructure.entities.jooq.Indexes;
import za.co.giraffe.infrastructure.entities.jooq.Keys;
import za.co.giraffe.infrastructure.entities.jooq.LearningManagementSystem;
import za.co.giraffe.infrastructure.entities.jooq.tables.records.JobseekerCourseRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JobseekerCourse extends TableImpl<JobseekerCourseRecord> {

    private static final long serialVersionUID = -1091085427;

    /**
     * The reference instance of <code>learning_management_system.jobseeker_course</code>
     */
    public static final JobseekerCourse JOBSEEKER_COURSE = new JobseekerCourse();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<JobseekerCourseRecord> getRecordType() {
        return JobseekerCourseRecord.class;
    }

    /**
     * The column <code>learning_management_system.jobseeker_course.id</code>.
     */
    public final TableField<JobseekerCourseRecord, Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.field("nextval('learning_management_system.jobseeker_course_id_seq'::regclass)", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * The column <code>learning_management_system.jobseeker_course.jobseeker_id</code>.
     */
    public final TableField<JobseekerCourseRecord, Integer> JOBSEEKER_ID = createField("jobseeker_id", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>learning_management_system.jobseeker_course.course_id</code>.
     */
    public final TableField<JobseekerCourseRecord, Integer> COURSE_ID = createField("course_id", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>learning_management_system.jobseeker_course.current_chapter_chronology</code>.
     */
    public final TableField<JobseekerCourseRecord, Integer> CURRENT_CHAPTER_CHRONOLOGY = createField("current_chapter_chronology", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>learning_management_system.jobseeker_course.max_completed_chapter_chronology</code>.
     */
    public final TableField<JobseekerCourseRecord, Integer> MAX_COMPLETED_CHAPTER_CHRONOLOGY = createField("max_completed_chapter_chronology", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>learning_management_system.jobseeker_course.total_chapter</code>.
     */
    public final TableField<JobseekerCourseRecord, Integer> TOTAL_CHAPTER = createField("total_chapter", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>learning_management_system.jobseeker_course.enrollment_date</code>.
     */
    public final TableField<JobseekerCourseRecord, OffsetDateTime> ENROLLMENT_DATE = createField("enrollment_date", org.jooq.impl.SQLDataType.TIMESTAMPWITHTIMEZONE, this, "");

    /**
     * The column <code>learning_management_system.jobseeker_course.last_updated_date</code>.
     */
    public final TableField<JobseekerCourseRecord, OffsetDateTime> LAST_UPDATED_DATE = createField("last_updated_date", org.jooq.impl.SQLDataType.TIMESTAMPWITHTIMEZONE, this, "");

    /**
     * Create a <code>learning_management_system.jobseeker_course</code> table reference
     */
    public JobseekerCourse() {
        this(DSL.name("jobseeker_course"), null);
    }

    /**
     * Create an aliased <code>learning_management_system.jobseeker_course</code> table reference
     */
    public JobseekerCourse(String alias) {
        this(DSL.name(alias), JOBSEEKER_COURSE);
    }

    /**
     * Create an aliased <code>learning_management_system.jobseeker_course</code> table reference
     */
    public JobseekerCourse(Name alias) {
        this(alias, JOBSEEKER_COURSE);
    }

    private JobseekerCourse(Name alias, Table<JobseekerCourseRecord> aliased) {
        this(alias, aliased, null);
    }

    private JobseekerCourse(Name alias, Table<JobseekerCourseRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> JobseekerCourse(Table<O> child, ForeignKey<O, JobseekerCourseRecord> key) {
        super(child, key, JOBSEEKER_COURSE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return LearningManagementSystem.LEARNING_MANAGEMENT_SYSTEM;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.JOBSEEKER_COURSE_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<JobseekerCourseRecord, Integer> getIdentity() {
        return Keys.IDENTITY_JOBSEEKER_COURSE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<JobseekerCourseRecord> getPrimaryKey() {
        return Keys.JOBSEEKER_COURSE_PKEY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<JobseekerCourseRecord>> getKeys() {
        return Arrays.<UniqueKey<JobseekerCourseRecord>>asList(Keys.JOBSEEKER_COURSE_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<JobseekerCourseRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<JobseekerCourseRecord, ?>>asList(Keys.JOBSEEKER_COURSE__JOBSEEKER_COURSE_COURSE_ID_FK);
    }

    public Course course() {
        return new Course(this, Keys.JOBSEEKER_COURSE__JOBSEEKER_COURSE_COURSE_ID_FK);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JobseekerCourse as(String alias) {
        return new JobseekerCourse(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JobseekerCourse as(Name alias) {
        return new JobseekerCourse(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public JobseekerCourse rename(String name) {
        return new JobseekerCourse(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public JobseekerCourse rename(Name name) {
        return new JobseekerCourse(name, null);
    }
}
