/*
 * This file is generated by jOOQ.
 */
package za.co.giraffe.infrastructure.entities.jooq.tables.pojos;


import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.UUID;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Category implements Serializable {

    private static final long serialVersionUID = -1217110651;

    private Integer        id;
    private String         name;
    private String         description;
    private UUID           fileId;
    private String         color;
    private Boolean        published;
    private OffsetDateTime createdDate;
    private OffsetDateTime publishedDate;

    public Category() {}

    public Category(Category value) {
        this.id = value.id;
        this.name = value.name;
        this.description = value.description;
        this.fileId = value.fileId;
        this.color = value.color;
        this.published = value.published;
        this.createdDate = value.createdDate;
        this.publishedDate = value.publishedDate;
    }

    public Category(
        Integer        id,
        String         name,
        String         description,
        UUID           fileId,
        String         color,
        Boolean        published,
        OffsetDateTime createdDate,
        OffsetDateTime publishedDate
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.fileId = fileId;
        this.color = color;
        this.published = published;
        this.createdDate = createdDate;
        this.publishedDate = publishedDate;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getFileId() {
        return this.fileId;
    }

    public void setFileId(UUID fileId) {
        this.fileId = fileId;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Boolean getPublished() {
        return this.published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public OffsetDateTime getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public OffsetDateTime getPublishedDate() {
        return this.publishedDate;
    }

    public void setPublishedDate(OffsetDateTime publishedDate) {
        this.publishedDate = publishedDate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Category (");

        sb.append(id);
        sb.append(", ").append(name);
        sb.append(", ").append(description);
        sb.append(", ").append(fileId);
        sb.append(", ").append(color);
        sb.append(", ").append(published);
        sb.append(", ").append(createdDate);
        sb.append(", ").append(publishedDate);

        sb.append(")");
        return sb.toString();
    }
}
