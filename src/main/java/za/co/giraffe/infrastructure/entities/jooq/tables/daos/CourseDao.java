/*
 * This file is generated by jOOQ.
 */
package za.co.giraffe.infrastructure.entities.jooq.tables.daos;


import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;

import za.co.giraffe.infrastructure.entities.jooq.tables.Course;
import za.co.giraffe.infrastructure.entities.jooq.tables.records.CourseRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class CourseDao extends DAOImpl<CourseRecord, za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course, Integer> {

    /**
     * Create a new CourseDao without any configuration
     */
    public CourseDao() {
        super(Course.COURSE, za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course.class);
    }

    /**
     * Create a new CourseDao with an attached configuration
     */
    public CourseDao(Configuration configuration) {
        super(Course.COURSE, za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Integer getId(za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course> fetchById(Integer... values) {
        return fetch(Course.COURSE.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course fetchOneById(Integer value) {
        return fetchOne(Course.COURSE.ID, value);
    }

    /**
     * Fetch records that have <code>name IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course> fetchByName(String... values) {
        return fetch(Course.COURSE.NAME, values);
    }

    /**
     * Fetch records that have <code>overview IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course> fetchByOverview(String... values) {
        return fetch(Course.COURSE.OVERVIEW, values);
    }

    /**
     * Fetch records that have <code>published IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course> fetchByPublished(Boolean... values) {
        return fetch(Course.COURSE.PUBLISHED, values);
    }

    /**
     * Fetch records that have <code>created_date IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course> fetchByCreatedDate(OffsetDateTime... values) {
        return fetch(Course.COURSE.CREATED_DATE, values);
    }

    /**
     * Fetch records that have <code>published_date IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course> fetchByPublishedDate(OffsetDateTime... values) {
        return fetch(Course.COURSE.PUBLISHED_DATE, values);
    }

    /**
     * Fetch records that have <code>category_id IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course> fetchByCategoryId(Integer... values) {
        return fetch(Course.COURSE.CATEGORY_ID, values);
    }

    /**
     * Fetch records that have <code>file_id IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course> fetchByFileId(UUID... values) {
        return fetch(Course.COURSE.FILE_ID, values);
    }

    /**
     * Fetch records that have <code>time_to_read IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course> fetchByTimeToRead(Integer... values) {
        return fetch(Course.COURSE.TIME_TO_READ, values);
    }
}
