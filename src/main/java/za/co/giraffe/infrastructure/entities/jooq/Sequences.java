/*
 * This file is generated by jOOQ.
 */
package za.co.giraffe.infrastructure.entities.jooq;


import javax.annotation.Generated;

import org.jooq.Sequence;
import org.jooq.impl.SequenceImpl;


/**
 * Convenience access to all sequences in learning_management_system
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Sequences {

    /**
     * The sequence <code>learning_management_system.category_id_seq</code>
     */
    public static final Sequence<Integer> CATEGORY_ID_SEQ = new SequenceImpl<Integer>("category_id_seq", LearningManagementSystem.LEARNING_MANAGEMENT_SYSTEM, org.jooq.impl.SQLDataType.INTEGER.nullable(false));

    /**
     * The sequence <code>learning_management_system.course_chapter_id_seq</code>
     */
    public static final Sequence<Integer> COURSE_CHAPTER_ID_SEQ = new SequenceImpl<Integer>("course_chapter_id_seq", LearningManagementSystem.LEARNING_MANAGEMENT_SYSTEM, org.jooq.impl.SQLDataType.INTEGER.nullable(false));

    /**
     * The sequence <code>learning_management_system.course_id_seq</code>
     */
    public static final Sequence<Integer> COURSE_ID_SEQ = new SequenceImpl<Integer>("course_id_seq", LearningManagementSystem.LEARNING_MANAGEMENT_SYSTEM, org.jooq.impl.SQLDataType.INTEGER.nullable(false));

    /**
     * The sequence <code>learning_management_system.course_review_id_seq</code>
     */
    public static final Sequence<Integer> COURSE_REVIEW_ID_SEQ = new SequenceImpl<Integer>("course_review_id_seq", LearningManagementSystem.LEARNING_MANAGEMENT_SYSTEM, org.jooq.impl.SQLDataType.INTEGER.nullable(false));

    /**
     * The sequence <code>learning_management_system.jobseeker_course_id_seq</code>
     */
    public static final Sequence<Integer> JOBSEEKER_COURSE_ID_SEQ = new SequenceImpl<Integer>("jobseeker_course_id_seq", LearningManagementSystem.LEARNING_MANAGEMENT_SYSTEM, org.jooq.impl.SQLDataType.INTEGER.nullable(false));
}
