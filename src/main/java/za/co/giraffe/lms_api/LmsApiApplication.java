/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.ControllerAdvice;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import za.co.giraffe.lms_api.configuration.FileConfig;

/**
 * @author Suraj Tekchandani
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@EnableConfigurationProperties({FileConfig.class})
@EnableSwagger2
@ComponentScan(basePackages = {
        "za.co.giraffe",
        "za.co.giraffe.lms_api.api",
        "za.co.giraffe.lms_api.configuration",
        "za.co.giraffe.lms_api.exception"
})
@ControllerAdvice("za.co.giraffe.lms_api.exception.RestExceptionHandler")
public class LmsApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LmsApiApplication.class, args);
    }

}
