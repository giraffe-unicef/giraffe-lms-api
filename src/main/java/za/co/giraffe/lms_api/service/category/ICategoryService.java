/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.service.category;

import za.co.giraffe.lms_api.model.lms.CategoryResponse;
import za.co.giraffe.lms_api.model.request.LmsCategoryRequest;
import za.co.giraffe.lms_api.model.request.PublishRequest;

import java.util.List;

/**
 * @author Suraj Tekchandani
 */
public interface ICategoryService {

    /**
     * Category service
     */
    CategoryResponse saveCategory(LmsCategoryRequest lmsCategoryRequest);

    void publishUnPublishCategory(Integer categoryId, PublishRequest publishRequest);

    List<CategoryResponse> getCategories();

    List<CategoryResponse> getCategoriesForJobseeker();

    CategoryResponse getCategoryById(Integer id);

    CategoryResponse updateCategory(Integer id, LmsCategoryRequest lmsCategoryRequest);

}
