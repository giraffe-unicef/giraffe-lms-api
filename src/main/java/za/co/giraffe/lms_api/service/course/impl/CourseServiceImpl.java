/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.service.course.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.CourseChapter;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.CourseReview;
import za.co.giraffe.lms_api.configuration.FileConfig;
import za.co.giraffe.lms_api.data.course.ICourseDAO;
import za.co.giraffe.lms_api.data.course_chapter.ICourseChapterDAO;
import za.co.giraffe.lms_api.exception.BadRequestException;
import za.co.giraffe.lms_api.model.lms.CourseChapterResponse;
import za.co.giraffe.lms_api.model.lms.CourseOverview;
import za.co.giraffe.lms_api.model.lms.CourseResponse;
import za.co.giraffe.lms_api.model.lms.FileUploadResponse;
import za.co.giraffe.lms_api.model.request.*;
import za.co.giraffe.lms_api.service.course.ICourseService;
import za.co.giraffe.lms_api.service.file.IFileService;
import za.co.giraffe.lms_api.util.LMSUtils;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author Suraj Tekchandani
 */
@Service
public class CourseServiceImpl implements ICourseService {

    private ICourseDAO iCourseDAO;
    private IFileService fileService;
    private FileConfig fileConfig;
    private ICourseChapterDAO iCourseChapterDAO;

    @Autowired
    public CourseServiceImpl(ICourseDAO iCourseDAO,
                             IFileService fileService,
                             FileConfig fileConfig,
                             ICourseChapterDAO iCourseChapterDAO) {
        this.iCourseDAO = iCourseDAO;
        this.fileService = fileService;
        this.fileConfig = fileConfig;
        this.iCourseChapterDAO = iCourseChapterDAO;
    }

    @Override
    public CourseResponse saveCourse(CourseRequest courseRequestRequest) {
        Course course = new Course();
        course.setCategoryId(courseRequestRequest.getCategoryId());
        course.setName(courseRequestRequest.getName());
        course.setOverview(courseRequestRequest.getOverview());
        course.setPublished(courseRequestRequest.getPublished());
        OffsetDateTime currentTimestamp = LMSUtils.getCurrentTimestamp();
        course.setCreatedDate(currentTimestamp);
        if (courseRequestRequest.getFile() != null) {
            FileUploadRequest fileUploadRequest = new FileUploadRequest();
            fileUploadRequest.setFile(courseRequestRequest.getFile());
            fileUploadRequest.setFileType(courseRequestRequest.getFileType());
            FileUploadResponse response = fileService.uploadFile(fileUploadRequest);
            if (response != null) {
                course.setFileId(response.getFileId());
            }
        }
        course = iCourseDAO.saveCourse(course);
        final int courseId = course.getId();
        courseRequestRequest.getChapters().forEach(courseChapterRequest -> {
            CourseChapter courseChapter = new CourseChapter();
            courseChapter.setChronology(courseChapterRequest.getChronology());
            courseChapter.setContent(courseChapterRequest.getContent());
            courseChapter.setName(courseChapterRequest.getName());
            courseChapter = iCourseChapterDAO.saveCourseChapter(courseChapter, courseId);
            courseChapterRequest.setId(courseChapter.getId());
        });

        CourseResponse response = createResponseFromCourse(course, courseRequestRequest.getChapters());
        /**
         * if we get request to publish course at time of creating course.
         */
        if (courseRequestRequest.getPublished()) {
            Long timeToRead = calculateTimeToReadForCourse(response);
            iCourseDAO.publishUnPublishCourse(courseId, Boolean.TRUE, timeToRead);
            response.setTimeToRead(timeToRead.intValue());
            response.setPublishedDate(currentTimestamp);
        }

        return response;
    }

    @Override
    public CourseResponse updateCourse(CourseRequest courseRequestRequest, Integer Id) {
        CourseResponse courseResponse = getCourseById(Id);
        Course course = new Course();
        course.setId(Id);
        if (courseResponse == null) {
            throw new BadRequestException("Course dose not exist");
        }
        /*if (courseResponse.getPublished()) {
            throw new BadRequestException("Published course cannot be updated");
        }*/

        course.setName(courseRequestRequest.getName());
        course.setOverview(courseRequestRequest.getOverview());

        if (courseRequestRequest.getCategoryId() != 0 && (courseResponse.getCategoryId() != courseRequestRequest.getCategoryId())) {
            course.setCategoryId(courseRequestRequest.getCategoryId());
        }

        if (courseRequestRequest.getFile() != null) {
            if (courseResponse.getFileId() == null) {
                FileUploadRequest fileUploadRequest = new FileUploadRequest();
                fileUploadRequest.setFile(courseRequestRequest.getFile());
                fileUploadRequest.setFileType(courseRequestRequest.getFileType());
                FileUploadResponse fileUploadResponse = fileService.uploadFile(fileUploadRequest);
                course.setFileId(fileUploadResponse.getFileId());
            } else {
                FileUploadRequest fileUploadRequest = new FileUploadRequest();
                fileUploadRequest.setFile(courseRequestRequest.getFile());
                fileUploadRequest.setFileType(courseRequestRequest.getFileType());
                fileService.replaceFile(fileUploadRequest, courseResponse.getFileId());
            }

        }
        iCourseDAO.updateCourse(course);
        courseRequestRequest.getChapters().stream().forEach(courseChapterRequest -> {
            try {
                CourseChapter courseChapter = new CourseChapter();
                if (courseChapterRequest.getId() != null && courseChapterRequest.getId() != 0) {
                    courseChapter.setId(courseChapterRequest.getId());
                    courseChapter.setChronology(courseChapterRequest.getChronology());
                    courseChapter.setContent(courseChapterRequest.getContent());
                    courseChapter.setName(courseChapterRequest.getName());
                    iCourseChapterDAO.updateCourseChapter(courseChapter);
                } else {
                    courseChapter.setChronology(courseChapterRequest.getChronology());
                    courseChapter.setContent(courseChapterRequest.getContent());
                    courseChapter.setName(courseChapterRequest.getName());
                    courseChapter = iCourseChapterDAO.saveCourseChapter(courseChapter, Id);
                    courseChapterRequest.setId(courseChapter.getId());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        if (courseRequestRequest.getDeletedChapters() != null && !courseRequestRequest.getDeletedChapters().isEmpty()) {
            courseRequestRequest.getDeletedChapters().stream().forEach(chapterId -> {
                try {
                    iCourseChapterDAO.deleteCourseChapterById(chapterId, Id);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });
        }
        CourseResponse response = getCourseById(Id);
        /**
         * if we get request to publish course at time of updating course.
         */
        if (courseRequestRequest.getPublished()) {
            Long timeToRead = calculateTimeToReadForCourse(response);
            response.setTimeToRead(timeToRead.intValue());
            response.setPublishedDate(LMSUtils.getCurrentTimestamp());
            iCourseDAO.publishUnPublishCourse(response.getId(), Boolean.TRUE, timeToRead);
        }

        return getCourseById(Id);
    }

    @Override
    public List<CourseResponse> fetchCourses() {
        List<CourseResponse> courseResponses = iCourseDAO.getCourse();
        courseResponses.parallelStream().forEach(courseResponse -> {
            courseResponse.setFileBaseUrl(fileConfig.getFile_url());
            courseResponse.setChapters(iCourseChapterDAO.getCourseChapterByChapterId(courseResponse.getId()));
        });
        return courseResponses;
    }

    @Override
    public CourseResponse getCourseById(Integer Id) {
        Optional<CourseResponse> courseResponseOptional = iCourseDAO.getCourseById(Id);
        if (courseResponseOptional.isPresent()) {
            CourseResponse courseResponse = courseResponseOptional.get();
            courseResponse.setFileBaseUrl(fileConfig.getFile_url());
            courseResponse.setChapters(iCourseChapterDAO.getCourseChapterByChapterId(courseResponse.getId()));
            return courseResponse;
        } else {
            throw new BadRequestException("Course not found");
        }

    }

    @Override
    public void publishUnPublishCourse(Integer courseId, PublishRequest publishRequest) {
        Long timeToRead = null;
        if (publishRequest.getPublished()) {
            CourseResponse courseResponse = getCourseById(courseId);
            timeToRead = calculateTimeToReadForCourse(courseResponse);
        }

        iCourseDAO.publishUnPublishCourse(courseId, publishRequest.getPublished(), timeToRead);
    }

    @Override
    public List<CourseOverview> browseCourse(Integer categoryId) {
        List<CourseOverview> courseOverviewList = iCourseDAO.browseCourse(categoryId);
        courseOverviewList.parallelStream().forEach(courseOverview -> {
            courseOverview.setFileBaseUrl(fileConfig.getFile_url());
            Integer likes = iCourseDAO.getLikesCountFromCourseId(courseOverview.getId());
            if (likes == null) {
                likes = 0;
            }
            courseOverview.setLikes(likes);
            courseOverview.setChapters(iCourseChapterDAO.getChapterOverviewByChapterId(courseOverview.getId()));
        });
        return courseOverviewList;
    }

    @Override
    public void reviewCourse(Integer courseId, CourseReviewRequest courseReviewRequest) {
        CourseReview courseReview = new CourseReview();
        courseReview.setCourseId(courseId);
        courseReview.setEmail(courseReviewRequest.getEmail());
        courseReview.setCreatedDate(LMSUtils.getCurrentTimestamp());
        if (courseReviewRequest.getRating() != null) {
            if (courseReviewRequest.getRating() == 3 || courseReviewRequest.getRating() == 4 || courseReviewRequest.getRating() == 5) {
                courseReview.setRating(courseReviewRequest.getRating());
                courseReview.setLike(true);
            } else {
                courseReview.setRating(courseReviewRequest.getRating());
                courseReview.setLike(false);
            }
        }
        courseReview.setReview(courseReviewRequest.getReview());
        iCourseDAO.upsertCourseReview(courseReview);
    }

    @Override
    public CourseOverview browseCourseById(Integer Id) {
        CourseOverview courseOverview = iCourseDAO.browseCourseById(Id);
        if (courseOverview != null) {
            courseOverview.setFileBaseUrl(fileConfig.getFile_url());
            Integer likes = iCourseDAO.getLikesCountFromCourseId(courseOverview.getId());
            if (likes == null) {
                likes = 0;
            }
            courseOverview.setLikes(likes);
            courseOverview.setChapters(iCourseChapterDAO.getChapterOverviewByChapterId(courseOverview.getId()));
        }
        return courseOverview;
    }

    private CourseResponse createResponseFromCourse(Course course, List<CourseChapterRequest> chapters) {
        CourseResponse courseResponse = new CourseResponse();
        courseResponse.setId(course.getId());
        courseResponse.setCategoryId(course.getCategoryId());
        courseResponse.setName(course.getName());
        courseResponse.setOverview(course.getOverview());
        courseResponse.setPublished(course.getPublished());
        courseResponse.setPublishedDate(course.getPublishedDate());
        courseResponse.setCreatedDate(course.getCreatedDate());
        courseResponse.setFileBaseUrl(fileConfig.getFile_url());
        courseResponse.setFileId(course.getFileId());
        List<CourseChapterResponse> courseChapterResponses = new ArrayList<>();
        chapters.parallelStream().forEach(courseChapterRequest -> {
            courseChapterResponses.add(new CourseChapterResponse(courseChapterRequest.getId(),
                    courseChapterRequest.getName(),
                    courseChapterRequest.getContent(),
                    courseChapterRequest.getChronology()));

        });
        courseResponse.setChapters(courseChapterResponses);
        return courseResponse;
    }

    private long calculateTimeToReadForCourse(CourseResponse courseResponse) {
        List<String> lines = new ArrayList<>();
        double wordCount = 0;
        // this is words per minlute
        double averageReadingSpeed = 150;
        lines.add(courseResponse.getOverview());
        courseResponse.getChapters().parallelStream().forEach(courseChapterResponse -> {
            lines.add(courseChapterResponse.getContent());
        });
        wordCount = lines.parallelStream().flatMap(line -> Arrays.stream(line.split(" "))).count();
        double ans = Math.ceil(wordCount / averageReadingSpeed);
        return Math.round(ans);
    }
}
