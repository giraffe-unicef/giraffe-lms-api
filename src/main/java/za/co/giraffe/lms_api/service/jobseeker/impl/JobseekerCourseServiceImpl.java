/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.service.jobseeker.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.JobseekerCourse;
import za.co.giraffe.lms_api.configuration.FileConfig;
import za.co.giraffe.lms_api.data.course.ICourseDAO;
import za.co.giraffe.lms_api.data.course_chapter.ICourseChapterDAO;
import za.co.giraffe.lms_api.data.jobseeker.IJobseekerDAO;
import za.co.giraffe.lms_api.exception.BadRequestException;
import za.co.giraffe.lms_api.model.jobseeker.CourseStatusResponse;
import za.co.giraffe.lms_api.model.jobseeker.EnrollCourseResponse;
import za.co.giraffe.lms_api.model.jobseeker.JobseekerEnrollmentStatus;
import za.co.giraffe.lms_api.model.lms.CourseChapterResponse;
import za.co.giraffe.lms_api.model.lms.CourseOverview;
import za.co.giraffe.lms_api.model.request.UpdateCourseStatusRequest;
import za.co.giraffe.lms_api.service.jobseeker.IJobseekerCourseService;
import za.co.giraffe.lms_api.util.LMSUtils;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

/**
 * @author Suraj Tekchandani
 */
@Service
public class JobseekerCourseServiceImpl implements IJobseekerCourseService {

    private IJobseekerDAO iJobseekerDAO;
    private ICourseChapterDAO iCourseChapterDAO;
    private FileConfig fileConfig;
    private ICourseDAO iCourseDAO;

    @Autowired
    public JobseekerCourseServiceImpl(IJobseekerDAO iJobseekerDAO,
                                      ICourseChapterDAO iCourseChapterDAO,
                                      FileConfig fileConfig,
                                      ICourseDAO iCourseDAO) {
        this.iJobseekerDAO = iJobseekerDAO;
        this.iCourseChapterDAO = iCourseChapterDAO;
        this.fileConfig = fileConfig;
        this.iCourseDAO = iCourseDAO;
    }

    @Override
    public EnrollCourseResponse enrollCourse(Integer courseId, Integer jobseekerId) {

        // todo to check if user i s already enrollrd or not
        Optional<JobseekerCourse> optionalJobseekerCourse =
                iJobseekerDAO.checkIfJobseekerHasAlreadyEnrolled(jobseekerId, courseId);

        if (optionalJobseekerCourse.isPresent()) {
            throw new BadRequestException("You have already enrolled this course");
        }
        Integer totalChapter = iCourseChapterDAO.getTotalNumberOfChapterByCourseId(courseId);
        JobseekerCourse jobseekerCourse = new JobseekerCourse();
        jobseekerCourse.setCourseId(courseId);
        jobseekerCourse.setJobseekerId(jobseekerId);
        jobseekerCourse.setCurrentChapterChronology(1);
        jobseekerCourse.setMaxCompletedChapterChronology(0);
        OffsetDateTime currentTimestamp = LMSUtils.getCurrentTimestamp();
        jobseekerCourse.setEnrollmentDate(currentTimestamp);
        jobseekerCourse.setLastUpdatedDate(currentTimestamp);
        jobseekerCourse.setTotalChapter(totalChapter);
        jobseekerCourse = iJobseekerDAO.enrollCourse(jobseekerCourse);
        return getEnrollCourseResponseFromJobseekerCourse(jobseekerCourse);
    }

    @Override
    public List<EnrollCourseResponse> getEnrolledCourseList(Integer jobseekerId) {
        List<EnrollCourseResponse> list = iJobseekerDAO.getEnrolledCourseList(jobseekerId);
        list.parallelStream().forEach(enrollCourseResponse -> {
            enrollCourseResponse.setFileBaseUrl(fileConfig.getFile_url());
        });
        return list;
    }

    @Override
    public void updateCourseStatus(Integer enrollmentId, UpdateCourseStatusRequest updateCourseStatusRequest) {
        Optional<JobseekerCourse> jobseekerCourseOptional = iJobseekerDAO.getEnrollCourseStatus(enrollmentId);
        if (jobseekerCourseOptional.isPresent()) {
            JobseekerCourse jobseekerCourse = jobseekerCourseOptional.get();
            boolean updateCompleteChapter = false;
            if (updateCourseStatusRequest.getCompletedChapter() > jobseekerCourse.getTotalChapter()) {
                throw new BadRequestException("Complete Chapter can not be greater than total chapters");
            }
            if (updateCourseStatusRequest.getCompletedChapter() > jobseekerCourse.getMaxCompletedChapterChronology() &&
                    updateCourseStatusRequest.getCompletedChapter() <= jobseekerCourse.getTotalChapter()) {
                updateCompleteChapter = true;
            }
            iJobseekerDAO.updateEnrollCourseStatus(enrollmentId, updateCourseStatusRequest, updateCompleteChapter);
        } else {
            throw new BadRequestException("Course enrollment not found");
        }

    }

    @Override
    public CourseStatusResponse getEnrollmentStatus(Integer enrollmentId) {
        Optional<JobseekerCourse> jobseekerCourseOptional = iJobseekerDAO.getEnrollCourseStatus(enrollmentId);
        if (jobseekerCourseOptional.isPresent()) {
            CourseStatusResponse courseStatusResponse = getCourseStatusResponseFromJobseekerCourse(jobseekerCourseOptional.get());
            List<CourseChapterResponse> chapterResponse = iCourseChapterDAO.getCourseChapterByChapterId(courseStatusResponse.getCourseId());
            courseStatusResponse.setCourseChapterResponse(chapterResponse);
            return courseStatusResponse;
        } else {
            throw new BadRequestException("Course enrollment not found");
        }
    }

    @Override
    public JobseekerEnrollmentStatus checkIfJobseekerHasEnrolledCourse(Integer jobseekerId, Integer courseId) {
        JobseekerEnrollmentStatus jobseekerEnrollmentStatus = new JobseekerEnrollmentStatus();
        Optional<JobseekerCourse> optionalJobseekerCourse =
                iJobseekerDAO.checkIfJobseekerHasAlreadyEnrolled(jobseekerId, courseId);
        if (optionalJobseekerCourse.isPresent()) {
            jobseekerEnrollmentStatus.setEnrolled(true);
            jobseekerEnrollmentStatus.setEnrollmentId(optionalJobseekerCourse.get().getId());
        } else {
            jobseekerEnrollmentStatus.setEnrolled(false);
        }
        return jobseekerEnrollmentStatus;
    }

    private EnrollCourseResponse getEnrollCourseResponseFromJobseekerCourse(JobseekerCourse jobseekerCourse) {
        EnrollCourseResponse enrollCourseResponse = new EnrollCourseResponse();
        enrollCourseResponse.setEnrollmentId(jobseekerCourse.getId());
        enrollCourseResponse.setCourseId(jobseekerCourse.getCourseId());
        enrollCourseResponse.setEnrollmentDate(jobseekerCourse.getEnrollmentDate());
        enrollCourseResponse.setCurrentChapterChronology(jobseekerCourse.getCurrentChapterChronology());
        enrollCourseResponse.setMaxCompletedChapterChronology(jobseekerCourse.getMaxCompletedChapterChronology());
        enrollCourseResponse.setTotalChapter(jobseekerCourse.getTotalChapter());
        CourseOverview courseOverview = iCourseDAO.browseCourseById(jobseekerCourse.getCourseId());
        enrollCourseResponse.setFileBaseUrl(fileConfig.getFile_url());
        enrollCourseResponse.setFileId(courseOverview.getFileId());
        enrollCourseResponse.setCourseName(courseOverview.getName());
        return enrollCourseResponse;
    }

    private CourseStatusResponse getCourseStatusResponseFromJobseekerCourse(JobseekerCourse jobseekerCourse) {
        CourseStatusResponse courseStatusResponse = new CourseStatusResponse();
        courseStatusResponse.setEnrollmentId(jobseekerCourse.getId());
        courseStatusResponse.setCourseId(jobseekerCourse.getCourseId());
        courseStatusResponse.setEnrollmentDate(jobseekerCourse.getEnrollmentDate());
        courseStatusResponse.setCurrentChapterChronology(jobseekerCourse.getCurrentChapterChronology());
        courseStatusResponse.setMaxCompletedChapterChronology(jobseekerCourse.getMaxCompletedChapterChronology());
        courseStatusResponse.setTotalChapter(jobseekerCourse.getTotalChapter());
        courseStatusResponse.setLastUpdatedDate(jobseekerCourse.getLastUpdatedDate());
        CourseOverview courseOverview = iCourseDAO.browseCourseById(jobseekerCourse.getCourseId());
        courseStatusResponse.setFileBaseUrl(fileConfig.getFile_url());
        courseStatusResponse.setFileId(courseOverview.getFileId());
        courseStatusResponse.setCourseName(courseOverview.getName());
        return courseStatusResponse;
    }
}
