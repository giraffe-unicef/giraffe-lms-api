/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.service.category.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Category;
import za.co.giraffe.lms_api.configuration.FileConfig;
import za.co.giraffe.lms_api.data.category.ICategoryDAO;
import za.co.giraffe.lms_api.exception.BadRequestException;
import za.co.giraffe.lms_api.model.lms.CategoryResponse;
import za.co.giraffe.lms_api.model.lms.FileUploadResponse;
import za.co.giraffe.lms_api.model.request.FileUploadRequest;
import za.co.giraffe.lms_api.model.request.LmsCategoryRequest;
import za.co.giraffe.lms_api.model.request.PublishRequest;
import za.co.giraffe.lms_api.service.category.ICategoryService;
import za.co.giraffe.lms_api.service.file.IFileService;
import za.co.giraffe.lms_api.service.file.impl.FileServiceImpl;
import za.co.giraffe.lms_api.util.LMSUtils;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

/**
 * @author Suraj Tekchandani
 */
@Service
public class CategoryServiceImpl implements ICategoryService {

    private ICategoryDAO lmsRepository;
    private IFileService fileService;
    private FileConfig fileConfig;

    @Autowired
    public CategoryServiceImpl(ICategoryDAO lmsRepository,
                               FileServiceImpl fileService,
                               FileConfig fileConfig) {
        this.lmsRepository = lmsRepository;
        this.fileService = fileService;
        this.fileConfig = fileConfig;
    }


    @Override
    public CategoryResponse saveCategory(LmsCategoryRequest lmsCategoryRequest) {
        Category category = new Category();
        category.setName(lmsCategoryRequest.getName());
        category.setDescription(lmsCategoryRequest.getDescription());
        category.setColor(lmsCategoryRequest.getColor());
        category.setPublished(lmsCategoryRequest.getPublished());
        OffsetDateTime currentTimestamp = LMSUtils.getCurrentTimestamp();
        category.setCreatedDate(currentTimestamp);
        if (lmsCategoryRequest.getPublished()) {
            category.setPublishedDate(currentTimestamp);
        }
        if (lmsCategoryRequest.getLogo() != null) {
            FileUploadRequest fileUploadRequest = new FileUploadRequest();
            fileUploadRequest.setFile(lmsCategoryRequest.getLogo());
            fileUploadRequest.setFileType(lmsCategoryRequest.getFileType());
            FileUploadResponse response = fileService.uploadFile(fileUploadRequest);
            if (response != null) {
                category.setFileId(response.getFileId());
            }
        }
        category = lmsRepository.insertCategory(category);
        return getCategoryResponseFromCategory(category);
    }


    @Override
    public void publishUnPublishCategory(Integer categoryId, PublishRequest publishRequest) {
        lmsRepository.publishUnPublishCategory(categoryId, publishRequest.getPublished());
    }

    @Override
    public List<CategoryResponse> getCategories() {
        List<CategoryResponse> categoryResponses = lmsRepository.getCategories();
        categoryResponses.parallelStream().forEach(categoryResponse -> {
            categoryResponse.setFileUrl(fileConfig.getFile_url());
        });
        return categoryResponses;
    }

    @Override
    public List<CategoryResponse> getCategoriesForJobseeker() {
        List<CategoryResponse> categoryResponses = lmsRepository.getCategoriesForJobseeker();
        categoryResponses.parallelStream().forEach(categoryResponse -> {
            categoryResponse.setFileUrl(fileConfig.getFile_url());
        });
        return categoryResponses;
    }

    @Override
    public CategoryResponse getCategoryById(Integer id) {
        Optional<CategoryResponse> categoryResponse = lmsRepository.getCategoryById(id);
        if (categoryResponse.isPresent()) {
            CategoryResponse response = categoryResponse.get();
            response.setFileUrl(fileConfig.getFile_url());
            return response;
        } else {
            throw new BadRequestException("Category not found");
        }
    }

    @Override
    public CategoryResponse updateCategory(Integer id, LmsCategoryRequest lmsCategoryRequest) {
        CategoryResponse categoryResponse = getCategoryById(id);
        if (categoryResponse == null) {
            throw new BadRequestException("Category Dose not exist");
        }
        Category category = new Category();
        category.setId(id);
        category.setName(lmsCategoryRequest.getName());
        category.setDescription(lmsCategoryRequest.getDescription());
        category.setColor(lmsCategoryRequest.getColor());

        if (lmsCategoryRequest.getLogo() != null) {
            FileUploadRequest fileUploadRequest = new FileUploadRequest();
            fileUploadRequest.setFile(lmsCategoryRequest.getLogo());
            fileUploadRequest.setFileType(lmsCategoryRequest.getFileType());
            fileService.replaceFile(fileUploadRequest, categoryResponse.getFileId());
        }
        lmsRepository.updateCategory(category);

        CategoryResponse response = getCategoryById(id);
        response.setFileUrl(fileConfig.getFile_url());
        /**
         * if we get request to publish Category at time of updating Category.
         */
        if (lmsCategoryRequest.getPublished()) {
            response.setPublished(Boolean.TRUE);
            response.setPublishedDate(LMSUtils.getCurrentTimestamp());
            lmsRepository.publishUnPublishCategory(category.getId(), lmsCategoryRequest.getPublished());
        }

        return response;
    }

    private CategoryResponse getCategoryResponseFromCategory(Category category) {
        CategoryResponse categoryResponse = new CategoryResponse();
        categoryResponse.setId(category.getId());
        categoryResponse.setName(category.getName());
        categoryResponse.setDescription(category.getDescription());
        categoryResponse.setColor(category.getColor());
        categoryResponse.setFileId(category.getFileId());
        categoryResponse.setPublished(category.getPublished());
        categoryResponse.setPublishedDate(category.getPublishedDate());
        categoryResponse.setFileUrl(fileConfig.getFile_url());
        return categoryResponse;
    }
}
