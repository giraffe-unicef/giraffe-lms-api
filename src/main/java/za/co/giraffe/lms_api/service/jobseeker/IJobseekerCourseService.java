/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.service.jobseeker;

import za.co.giraffe.lms_api.model.jobseeker.CourseStatusResponse;
import za.co.giraffe.lms_api.model.jobseeker.EnrollCourseResponse;
import za.co.giraffe.lms_api.model.jobseeker.JobseekerEnrollmentStatus;
import za.co.giraffe.lms_api.model.request.UpdateCourseStatusRequest;

import java.util.List;

/**
 * @author Suraj Tekchandani
 */
public interface IJobseekerCourseService {

    EnrollCourseResponse enrollCourse(Integer courseId, Integer jobseekerId);

    List<EnrollCourseResponse> getEnrolledCourseList(Integer jobseekerId);

    void updateCourseStatus(Integer enrollmentId, UpdateCourseStatusRequest updateCourseStatusRequest);

    CourseStatusResponse getEnrollmentStatus(Integer enrollmentId);

    JobseekerEnrollmentStatus checkIfJobseekerHasEnrolledCourse(Integer jobseekerId, Integer courseId);
}
