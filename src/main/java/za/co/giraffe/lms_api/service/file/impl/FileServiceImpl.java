/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.service.file.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.FileUpload;
import za.co.giraffe.lms_api.configuration.FileConfig;
import za.co.giraffe.lms_api.data.file.IFileDAO;
import za.co.giraffe.lms_api.exception.BadRequestException;
import za.co.giraffe.lms_api.model.lms.FileUploadResponse;
import za.co.giraffe.lms_api.model.request.FileUploadRequest;
import za.co.giraffe.lms_api.service.file.IFileService;
import za.co.giraffe.lms_api.util.LMSUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Suraj Tekchandani
 */
@Service
public class FileServiceImpl implements IFileService {


    private IFileDAO IFileDAO;
    private FileConfig fileConfig;

    @Autowired
    public FileServiceImpl(IFileDAO IFileDAO,
                           FileConfig fileConfig) {
        this.IFileDAO = IFileDAO;
        this.fileConfig = fileConfig;
    }

    @Override
    public FileUploadResponse uploadFile(FileUploadRequest fileUploadRequest) {
        if (fileUploadRequest.getFileType() == null) {
            throw new BadRequestException("please provide file type");
        }
        try {
            UUID fileId = UUID.randomUUID();
            String filePath = fileConfig.getFile_path() + "/" + fileId.toString() + "." + (fileUploadRequest.getFileType().replace(".", ""));
            File target = new File(filePath);
            FileOutputStream fos = new FileOutputStream(target);
            fos.write(fileUploadRequest.getFile());
            fos.close();
            FileUpload fileUpload = new FileUpload();
            fileUpload.setFileId(fileId);
            fileUpload.setFilePath(filePath);
            fileUpload.setFileType(fileUploadRequest.getFileType());
            fileUpload.setCreatedDate(LMSUtils.getCurrentTimestamp());
            IFileDAO.uploadFile(fileUpload);
            return new FileUploadResponse(fileId, filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FileUploadResponse replaceFile(FileUploadRequest fileUploadRequest, UUID fileId) {
        try {
            String filePath = fileConfig.getFile_path() + "/" + fileId.toString() + "." + fileUploadRequest.getFileType();
            File target = new File(filePath);
            FileOutputStream fos = new FileOutputStream(target);
            fos.write(fileUploadRequest.getFile());
            fos.close();
            FileUpload fileUpload = new FileUpload();
            fileUpload.setFileId(fileId);
            fileUpload.setFilePath(filePath);
            fileUpload.setFileType(fileUploadRequest.getFileType());
            IFileDAO.replaceFile(fileUpload);
            return new FileUploadResponse(fileId, filePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Resource getFileAsResource(UUID fileId) {
        try {
            Optional<FileUpload> fileUploadOptional = IFileDAO.getFileFromFileId(fileId);
            if (fileUploadOptional.isPresent()) {
                FileUpload fileUpload = fileUploadOptional.get();
                Path path = Paths.get(fileUpload.getFilePath()).toAbsolutePath().normalize();
                Resource resource = new UrlResource(path.toUri());
                if (resource.exists()) {
                    return resource;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
