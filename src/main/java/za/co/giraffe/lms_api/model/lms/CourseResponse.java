/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.model.lms;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

/**
 * @author Suraj Tekchandani
 */
@Getter
@Setter
@NoArgsConstructor
public class CourseResponse {

    private Integer id;
    private String name;
    private String overview;
    private Boolean published = false;
    @JsonProperty("category_id")
    private Integer categoryId;
    @JsonProperty("category_name")
    private String categoryName;
    @JsonIgnore
    private UUID fileId;
    @JsonProperty("file_url")
    private String fileUrl;
    @JsonProperty("created_date")
    private OffsetDateTime createdDate;
    @JsonProperty("published_date")
    private OffsetDateTime publishedDate;
    @JsonIgnore
    private String fileBaseUrl;
    @JsonProperty("time_to_read")
    private Integer timeToRead;


    private List<CourseChapterResponse> chapters;

    public String getFileUrl() {
        if (fileId == null) {
            return null;
        }
        return fileBaseUrl + fileId;
    }
}
