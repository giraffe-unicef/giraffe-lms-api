
/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.data.category.impl;

import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Category;
import za.co.giraffe.infrastructure.entities.jooq.tables.records.CategoryRecord;
import za.co.giraffe.lms_api.data.category.ICategoryDAO;
import za.co.giraffe.lms_api.model.lms.CategoryResponse;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static za.co.giraffe.infrastructure.entities.jooq.tables.Category.CATEGORY;

/**
 * @author Suraj Tekchandani
 */
@Component
public class ICategoryDAOImpl implements ICategoryDAO {
    private DSLContext dsl;

    @Autowired
    public ICategoryDAOImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public Category insertCategory(Category category) {
        try {
            CategoryRecord record = dsl.newRecord(CATEGORY, category);
            record.store();
            category.setId(record.getId());
            return category;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public List<CategoryResponse> getCategories() {
        return dsl.select().from(CATEGORY).fetchInto(CategoryResponse.class);
    }

    @Override
    public void publishUnPublishCategory(Integer id, Boolean published) {
        Map<TableField, Object> map = new HashMap<>();
        map.put(CATEGORY.PUBLISHED, published);
        if (published) {
            map.put(CATEGORY.PUBLISHED_DATE, new Timestamp(System.currentTimeMillis()));
        }

        dsl.update(CATEGORY)
                .set(map)
                .where(CATEGORY.ID.equal(id))
                .execute();
    }

    @Override
    public List<CategoryResponse> getCategoriesForJobseeker() {
        return dsl.select()
                .from(CATEGORY)
                .where(CATEGORY.PUBLISHED.eq(true))
                .fetchInto(CategoryResponse.class);
    }

    @Override
    public Optional<CategoryResponse> getCategoryById(Integer Id) {
        return dsl.select()
                .from(CATEGORY)
                .where(CATEGORY.ID.eq(Id))
                .fetchOptionalInto(CategoryResponse.class);

    }

    @Override
    public void updateCategory(Category category) {
        Map<TableField, Object> map = new HashMap<>();
        map.put(CATEGORY.NAME, category.getName());
        map.put(CATEGORY.DESCRIPTION, category.getDescription());
        map.put(CATEGORY.COLOR, category.getColor());
        if (category.getFileId() != null) {
            map.put(CATEGORY.FILE_ID, category.getFileId());
        }

        dsl.update(CATEGORY)
                .set(map)
                .where(CATEGORY.ID.eq(category.getId()))
                .execute();
    }
}
