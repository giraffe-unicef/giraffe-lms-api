/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.data.course_chapter.impl;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.CourseChapter;
import za.co.giraffe.infrastructure.entities.jooq.tables.records.CourseChapterRecord;
import za.co.giraffe.lms_api.data.course_chapter.ICourseChapterDAO;
import za.co.giraffe.lms_api.model.lms.ChapterOverview;
import za.co.giraffe.lms_api.model.lms.CourseChapterResponse;

import java.util.List;

import static za.co.giraffe.infrastructure.entities.jooq.tables.CourseChapter.COURSE_CHAPTER;

/**
 * @author Suraj Tekchandani
 */
@Component
public class ICourseChapterDAOImpl implements ICourseChapterDAO {


    private DSLContext dsl;

    @Autowired
    public ICourseChapterDAOImpl(DSLContext dsl) {
        this.dsl = dsl;
    }


    @Override
    public CourseChapter saveCourseChapter(CourseChapter courseChapter, Integer courseId) {
        try {
            CourseChapterRecord record = dsl.newRecord(COURSE_CHAPTER, courseChapter);
            record.setCourseId(courseId);
            record.store();
            courseChapter.setId(record.getId());
            return courseChapter;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public List<CourseChapterResponse> getCourseChapterByChapterId(Integer courseId) {
        return dsl.select()
                .from(COURSE_CHAPTER)
                .where(COURSE_CHAPTER.COURSE_ID.eq(courseId))
                .fetchInto(CourseChapterResponse.class);
    }

    @Override
    public List<ChapterOverview> getChapterOverviewByChapterId(Integer courseId) {
        return dsl.select()
                .from(COURSE_CHAPTER)
                .where(COURSE_CHAPTER.COURSE_ID.eq(courseId))
                .fetchInto(ChapterOverview.class);
    }

    @Override
    public Integer getTotalNumberOfChapterByCourseId(Integer courseId) {
        return dsl.selectCount().from(COURSE_CHAPTER).where(COURSE_CHAPTER.COURSE_ID.eq(courseId)).fetchOneInto(Integer.class);
    }

    @Override
    public void updateCourseChapter(CourseChapter courseChapter) {

        dsl.update(COURSE_CHAPTER)
                .set(COURSE_CHAPTER.NAME, courseChapter.getName())
                .set(COURSE_CHAPTER.CONTENT, courseChapter.getContent())
                .set(COURSE_CHAPTER.CHRONOLOGY, courseChapter.getChronology())
                .where(COURSE_CHAPTER.ID.equal(courseChapter.getId()))
                .execute();
    }

    @Override
    public CourseChapterResponse getCourseChapterByChapterIdAndChronology(Integer courseId, Short chronology) {
        return dsl.select()
                .from(COURSE_CHAPTER)
                .where(COURSE_CHAPTER.COURSE_ID.eq(courseId))
                .and(COURSE_CHAPTER.CHRONOLOGY.eq(chronology))
                .fetchOneInto(CourseChapterResponse.class);
    }

    @Override
    public void deleteCourseChapterById(Integer courseChapterId, Integer courseId) {
        dsl.delete(COURSE_CHAPTER)
                .where(COURSE_CHAPTER.ID.eq(courseChapterId))
                .and(COURSE_CHAPTER.COURSE_ID.eq(courseId))
                .execute();
    }
}
