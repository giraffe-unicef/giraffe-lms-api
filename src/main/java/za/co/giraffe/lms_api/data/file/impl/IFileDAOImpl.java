/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.data.file.impl;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.FileUpload;
import za.co.giraffe.infrastructure.entities.jooq.tables.records.FileUploadRecord;
import za.co.giraffe.lms_api.data.file.IFileDAO;

import java.util.Optional;
import java.util.UUID;

import static za.co.giraffe.infrastructure.entities.jooq.tables.FileUpload.FILE_UPLOAD;

/**
 * @author Suraj Tekchandani
 */
@Component
public class IFileDAOImpl implements IFileDAO {

    private DSLContext dsl;

    @Autowired
    public IFileDAOImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public void uploadFile(FileUpload fileUpload) {
        try {
            FileUploadRecord record = dsl.newRecord(FILE_UPLOAD, fileUpload);
            record.store();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void replaceFile(FileUpload fileUpload) {
        dsl.update(FILE_UPLOAD)
                .set(FILE_UPLOAD.FILE_PATH, fileUpload.getFilePath())
                .set(FILE_UPLOAD.FILE_TYPE, fileUpload.getFileType())
                .where(FILE_UPLOAD.FILE_ID.equal(fileUpload.getFileId()))
                .execute();
    }

    @Override
    public Optional<FileUpload> getFileFromFileId(UUID fileId) {
        return dsl.select().from(FILE_UPLOAD).where(FILE_UPLOAD.FILE_ID.eq(fileId)).fetchOptionalInto(FileUpload.class);
    }

}
