/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.data.course.impl;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Course;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.CourseReview;
import za.co.giraffe.infrastructure.entities.jooq.tables.records.CourseRecord;
import za.co.giraffe.lms_api.data.course.ICourseDAO;
import za.co.giraffe.lms_api.model.lms.CourseOverview;
import za.co.giraffe.lms_api.model.lms.CourseResponse;

import java.sql.Timestamp;
import java.util.*;

import static org.jooq.impl.DSL.trueCondition;
import static za.co.giraffe.infrastructure.entities.jooq.tables.Category.CATEGORY;
import static za.co.giraffe.infrastructure.entities.jooq.tables.Course.COURSE;
import static za.co.giraffe.infrastructure.entities.jooq.tables.CourseReview.COURSE_REVIEW;

/**
 * @author Suraj Tekchandani
 */
@Component
public class ICourseDAOImpl implements ICourseDAO {

    private DSLContext dsl;

    @Autowired
    public ICourseDAOImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public Course saveCourse(Course course) {
        try {
            CourseRecord record = dsl.newRecord(COURSE, course);
            record.store();
            course.setId(record.getId());
            return course;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void updateCourse(Course course) {
        Map<TableField, Object> map = new HashMap<>();

        map.put(COURSE.NAME, course.getName());
        map.put(COURSE.OVERVIEW, course.getOverview());
        if (course.getFileId() != null) {
            map.put(COURSE.FILE_ID, course.getFileId());
        }
        if (course.getCategoryId() != null) {
            map.put(COURSE.CATEGORY_ID, course.getCategoryId());
        }

        dsl.update(COURSE)
                .set(map)
                .where(COURSE.ID.equal(course.getId()))
                .execute();
    }

    @Override
    public List<CourseResponse> getCourse() {
        return dsl.select().from(COURSE).fetchInto(CourseResponse.class);

    }

    @Override
    public Optional<CourseResponse> getCourseById(Integer Id) {
        return dsl.select(COURSE.ID)
                .select(COURSE.NAME)
                .select(COURSE.OVERVIEW)
                .select(COURSE.PUBLISHED)
                .select(COURSE.CATEGORY_ID)
                .select(CATEGORY.NAME.as("category_name"))
                .select(COURSE.FILE_ID)
                .select(COURSE.CREATED_DATE)
                .select(COURSE.PUBLISHED_DATE)
                .select(COURSE.TIME_TO_READ)
                .from(COURSE)
                .innerJoin(CATEGORY).on(COURSE.CATEGORY_ID.eq(CATEGORY.ID))
                .where(COURSE.ID.eq(Id))
                .fetchOptionalInto(CourseResponse.class);

    }

    @Override
    public void publishUnPublishCourse(Integer id, Boolean published, Long timeToRead) {
        Map<TableField, Object> map = new HashMap<>();
        map.put(COURSE.PUBLISHED, published);
        if (published) {
            map.put(COURSE.PUBLISHED_DATE, new Timestamp(System.currentTimeMillis()));
            map.put(COURSE.TIME_TO_READ, timeToRead);
        }

        dsl.update(COURSE)
                .set(map)
                .where(COURSE.ID.equal(id))
                .execute();
    }

    @Override
    public List<CourseOverview> browseCourse(Integer categoryId) {
        Condition condition = trueCondition();
        condition = condition.and(COURSE.PUBLISHED.eq(true));

        if (categoryId != null) {
            condition = condition.and(COURSE.CATEGORY_ID.eq(categoryId));
        }
        try {
            return dsl.select(COURSE.ID)
                    .select(COURSE.NAME)
                    .select(COURSE.OVERVIEW)
                    .select(COURSE.PUBLISHED)
                    .select(COURSE.CATEGORY_ID)
                    .select(CATEGORY.NAME.as("category_name"))
                    .select(CATEGORY.COLOR.as("category_color"))
                    .select(COURSE.FILE_ID)
                    .select(COURSE.CREATED_DATE)
                    .select(COURSE.PUBLISHED_DATE)
                    .select(COURSE.TIME_TO_READ)
                    .from(COURSE)
                    .innerJoin(CATEGORY).on(COURSE.CATEGORY_ID.eq(CATEGORY.ID))
                    .where(condition)
                    .fetchInto(CourseOverview.class);
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public void upsertCourseReview(CourseReview courseReview) {
        /*Optional<CourseReview> courseReviewOptional = dsl.select()
                .from(COURSE_REVIEW)
                .where(COURSE_REVIEW.JOBSEEKER_COURSE_ID.eq(courseReview.getJobseekerCourseId()))
                .fetchOptionalInto(CourseReview.class);
        if (courseReviewOptional.isPresent()) {
            dsl.update(COURSE_REVIEW)
                    .set(COURSE_REVIEW.LIKE, courseReview.getLike())
                    .set(COURSE_REVIEW.REVIEW, courseReview.getReview())
                    .set(COURSE_REVIEW.RATING, courseReview.getRating())
                    .where(COURSE_REVIEW.ID.eq(courseReviewOptional.get().getId()))
                    .execute();
        } else {*/
        try {
            dsl.newRecord(COURSE_REVIEW, courseReview).store();
        } catch (Exception e) {
            e.printStackTrace();

        }
        /* }*/

    }

    @Override
    public Integer getLikesCountFromCourseId(Integer courseId) {
        return dsl.selectCount()
                .from(COURSE)
                /*.leftJoin(JOBSEEKER_COURSE).on(COURSE.ID.eq(JOBSEEKER_COURSE.COURSE_ID))*/
                .leftJoin(COURSE_REVIEW).on(COURSE.ID.eq(COURSE_REVIEW.COURSE_ID))
                .where(COURSE_REVIEW.LIKE.eq(true))
                .and(COURSE.ID.eq(courseId))
                .fetchOneInto(Integer.class);
    }

    @Override
    public CourseOverview browseCourseById(Integer courseId) {
        return dsl.select(COURSE.ID)
                .select(COURSE.NAME)
                .select(COURSE.OVERVIEW)
                .select(COURSE.PUBLISHED)
                .select(COURSE.CATEGORY_ID)
                .select(CATEGORY.NAME.as("category_name"))
                .select(CATEGORY.COLOR.as("category_color"))
                .select(COURSE.FILE_ID)
                .select(COURSE.CREATED_DATE)
                .select(COURSE.PUBLISHED_DATE)
                .select(COURSE.TIME_TO_READ)
                .from(COURSE)
                .innerJoin(CATEGORY).on(COURSE.CATEGORY_ID.eq(CATEGORY.ID))
                .where(COURSE.ID.eq(courseId))
                .and(COURSE.PUBLISHED.eq(true))
                .fetchOneInto(CourseOverview.class);
    }
}
