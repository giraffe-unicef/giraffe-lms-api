/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.data.course_chapter;

import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.CourseChapter;
import za.co.giraffe.lms_api.model.lms.ChapterOverview;
import za.co.giraffe.lms_api.model.lms.CourseChapterResponse;

import java.util.List;

/**
 * @author Suraj Tekchandani
 */
public interface ICourseChapterDAO {
    CourseChapter saveCourseChapter(CourseChapter courseChapter, Integer courseId);

    List<CourseChapterResponse> getCourseChapterByChapterId(Integer courseId);

    List<ChapterOverview> getChapterOverviewByChapterId(Integer courseId);

    Integer getTotalNumberOfChapterByCourseId(Integer courseId);

    void updateCourseChapter(CourseChapter courseChapter);

    CourseChapterResponse getCourseChapterByChapterIdAndChronology(Integer courseId, Short chronology);

    void deleteCourseChapterById(Integer courseChapterId, Integer courseId);
}
