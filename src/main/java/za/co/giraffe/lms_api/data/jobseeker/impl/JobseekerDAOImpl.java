/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.data.jobseeker.impl;

import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.JobseekerCourse;
import za.co.giraffe.infrastructure.entities.jooq.tables.records.JobseekerCourseRecord;
import za.co.giraffe.lms_api.data.jobseeker.IJobseekerDAO;
import za.co.giraffe.lms_api.model.jobseeker.EnrollCourseResponse;
import za.co.giraffe.lms_api.model.request.UpdateCourseStatusRequest;
import za.co.giraffe.lms_api.util.LMSUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static za.co.giraffe.infrastructure.entities.jooq.tables.Course.COURSE;
import static za.co.giraffe.infrastructure.entities.jooq.tables.JobseekerCourse.JOBSEEKER_COURSE;

/**
 * @author Suraj Tekchandani
 */
@Service
@Transactional("transactionManager")
public class JobseekerDAOImpl implements IJobseekerDAO {
    private DSLContext dsl;

    @Autowired
    public JobseekerDAOImpl(DSLContext dsl) {
        this.dsl = dsl;
    }


    @Override
    public JobseekerCourse enrollCourse(JobseekerCourse jobseekerCourse) {
        try {
            JobseekerCourseRecord record = dsl.newRecord(JOBSEEKER_COURSE, jobseekerCourse);
            record.store();
            jobseekerCourse.setId(record.getId());
            return jobseekerCourse;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<EnrollCourseResponse> getEnrolledCourseList(Integer jobseekerId) {
        return dsl
                .select(JOBSEEKER_COURSE.ID.as("enrollment_id"))
                .select(JOBSEEKER_COURSE.COURSE_ID.as("course_id"))
                .select(JOBSEEKER_COURSE.ENROLLMENT_DATE.as("enrollment_date"))
                .select(JOBSEEKER_COURSE.LAST_UPDATED_DATE.as("last_updated_date"))
                .select(JOBSEEKER_COURSE.CURRENT_CHAPTER_CHRONOLOGY.as("current_chapter_chronology"))
                .select(JOBSEEKER_COURSE.MAX_COMPLETED_CHAPTER_CHRONOLOGY.as("max_completed_chapter_chronology"))
                .select(JOBSEEKER_COURSE.TOTAL_CHAPTER.as("total_chapter"))
                .select(COURSE.NAME.as("course_name"))
                .select(COURSE.FILE_ID.as("file_id"))
                .from(JOBSEEKER_COURSE)
                .innerJoin(COURSE).on(COURSE.ID.eq(JOBSEEKER_COURSE.COURSE_ID))
                .where(JOBSEEKER_COURSE.JOBSEEKER_ID.eq(jobseekerId))
                .and(COURSE.PUBLISHED.eq(true))
                .and(JOBSEEKER_COURSE.MAX_COMPLETED_CHAPTER_CHRONOLOGY.notEqual(JOBSEEKER_COURSE.TOTAL_CHAPTER))
                .orderBy(JOBSEEKER_COURSE.LAST_UPDATED_DATE.desc())
                .fetchInto(EnrollCourseResponse.class);
    }

    @Override
    public Optional<JobseekerCourse> checkIfJobseekerHasAlreadyEnrolled(Integer jobseekerId, Integer courseId) {
        return dsl.select()
                .from(JOBSEEKER_COURSE)
                .where(JOBSEEKER_COURSE.JOBSEEKER_ID.eq(jobseekerId))
                .and(JOBSEEKER_COURSE.COURSE_ID.eq(courseId))
                .fetchOptionalInto(JobseekerCourse.class);
    }

    @Override
    public Optional<JobseekerCourse> getEnrollCourseStatus(Integer enrollmentId) {
        return dsl.select()
                .from(JOBSEEKER_COURSE)
                .where(JOBSEEKER_COURSE.ID.eq(enrollmentId))
                .fetchOptionalInto(JobseekerCourse.class);
    }

    @Override
    public void updateEnrollCourseStatus(Integer enrollmentId, UpdateCourseStatusRequest updateCourseStatusRequest, Boolean updateCompleteChapter) {
        Map<TableField, Object> map = new HashMap<>();
        map.put(JOBSEEKER_COURSE.CURRENT_CHAPTER_CHRONOLOGY, updateCourseStatusRequest.getCurrentChapter());
        map.put(JOBSEEKER_COURSE.LAST_UPDATED_DATE, LMSUtils.getCurrentTimestamp());
        if (updateCompleteChapter) {
            map.put(JOBSEEKER_COURSE.MAX_COMPLETED_CHAPTER_CHRONOLOGY, updateCourseStatusRequest.getCompletedChapter());
        }

        dsl.update(JOBSEEKER_COURSE)
                .set(map)
                .where(JOBSEEKER_COURSE.ID.equal(enrollmentId))
                .execute();
    }
}
