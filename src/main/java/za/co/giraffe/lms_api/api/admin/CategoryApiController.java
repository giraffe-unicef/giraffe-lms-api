
/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.api.admin;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.giraffe.lms_api.model.lms.CategoryResponse;
import za.co.giraffe.lms_api.model.request.LmsCategoryRequest;
import za.co.giraffe.lms_api.model.request.PublishRequest;
import za.co.giraffe.lms_api.service.category.ICategoryService;

import java.io.IOException;

/**
 * @author Suraj Tekchandani
 */
@RestController
@RequestMapping("/admin/category")
@Api(value = "Admin API", tags = {"Admin API"})
public class CategoryApiController {
    private static final Logger log = LoggerFactory.getLogger(CategoryApiController.class);
    private ICategoryService iCategoryService;

    @Autowired
    public CategoryApiController(ICategoryService iCategoryService) {
        this.iCategoryService = iCategoryService;
    }

    @ApiOperation(value = "Create Lms Category", nickname = "createLmsCategory", notes = "", response = CategoryResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "successful operation", response = CategoryResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = String.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @PostMapping
    public ResponseEntity createLmsCategory(@RequestBody LmsCategoryRequest lmsCategoryRequest) {

        return new ResponseEntity<>(iCategoryService.saveCategory(lmsCategoryRequest), HttpStatus.OK);
    }


    @ApiOperation(value = "Publish or UnPublish Lms Category", nickname = "publishUnPublishCategory", notes = "", response = CategoryResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "successful operation", response = String.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @PatchMapping("/{Id}")
    public ResponseEntity publishUnPublishCategory(@ApiParam(value = "Category ID", required = true) @PathVariable("Id") Integer id,
                                                   @RequestBody PublishRequest publishRequest) {

        iCategoryService.publishUnPublishCategory(id, publishRequest);
        return new ResponseEntity<>("OK", HttpStatus.NO_CONTENT);

    }

    @ApiOperation(value = "Update Lms Category", nickname = "updateLmsCategory", notes = "", response = CategoryResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = CategoryResponse.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @PutMapping("/{Id}")
    public ResponseEntity updateLmsCategory(@ApiParam(value = "Category ID", required = true) @PathVariable("Id") Integer id,
                                            @RequestBody LmsCategoryRequest lmsCategoryRequest) throws IOException {
        return new ResponseEntity<>(iCategoryService.updateCategory(id, lmsCategoryRequest), HttpStatus.OK);
    }

    @ApiOperation(value = "Get Lms Category by id", nickname = "getLmsCategoryById", notes = "", response = CategoryResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = CategoryResponse.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @GetMapping("/{Id}")
    public ResponseEntity getLmsCategoryById(@ApiParam(value = "Category ID", required = true) @PathVariable("Id") Integer id) throws IOException {
        CategoryResponse categoryResponse = iCategoryService.getCategoryById(id);
        return new ResponseEntity<>(categoryResponse, HttpStatus.OK);
    }

    @ApiOperation(value = "Fetch Lms Category", nickname = "getLmsCategory", notes = "", response = CategoryResponse.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = CategoryResponse.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @GetMapping
    public ResponseEntity getLmsCategory() {
        return new ResponseEntity<>(iCategoryService.getCategories(), HttpStatus.OK);
    }


}
