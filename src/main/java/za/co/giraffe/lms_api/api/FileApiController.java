
/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.api;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.giraffe.lms_api.configuration.FileConfig;
import za.co.giraffe.lms_api.model.lms.FileUploadResponse;
import za.co.giraffe.lms_api.model.request.FileUploadRequest;
import za.co.giraffe.lms_api.service.file.IFileService;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @author Suraj Tekchandani
 */
@RestController
@RequestMapping("/file")
@Api(value = "File API", tags = {"File API"})
public class FileApiController {

    private static final Logger log = LoggerFactory.getLogger(FileApiController.class);
    private IFileService fileService;
    private FileConfig fileConfig;


    @Autowired
    public FileApiController(IFileService fileService,
                             FileConfig fileConfig) {
        this.fileService = fileService;
        this.fileConfig = fileConfig;

    }

    @ApiOperation(value = "Upload Lms Files", nickname = "uploadLmsFile", notes = "", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @RequestMapping(value = "/upload",
            method = RequestMethod.POST)
    public ResponseEntity uploadLmsFile(@RequestBody FileUploadRequest fileUploadRequest) throws IOException {
        try {
            FileUploadResponse fileUploadResponse = fileService.uploadFile(fileUploadRequest);
            String FileUrl = fileConfig.getFile_url() + fileUploadResponse.getFileId();
            return new ResponseEntity<>(FileUrl, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @ApiOperation(value = "Get Lms Files", nickname = "uploadLmsFile", notes = "", response = File.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = File.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @RequestMapping(value = "/{file_id}",
            produces = {"application/octet-stream"},
            method = RequestMethod.GET)
    public ResponseEntity getLmsFile(@ApiParam(value = "File ID", required = true) @PathVariable("file_id") UUID fileId, HttpServletRequest request) throws IOException {
        Resource resource = fileService.getFileAsResource(fileId);
        if (resource != null) {

            // Try to determine file's content type
            String contentType = null;
            try {
                contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            } catch (IOException ex) {
                log.info("Could not determine file type.");
            }
            // Fallback to the default content type if type could not be determined
            if (contentType == null) {
                contentType = "application/octet-stream";
            }
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);
        } else {
            return new ResponseEntity<>("File Not Found", HttpStatus.NOT_FOUND);
        }
    }
}
