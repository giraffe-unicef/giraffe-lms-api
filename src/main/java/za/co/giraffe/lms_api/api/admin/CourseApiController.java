
/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */

package za.co.giraffe.lms_api.api.admin;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.giraffe.lms_api.model.lms.CategoryResponse;
import za.co.giraffe.lms_api.model.lms.CourseResponse;
import za.co.giraffe.lms_api.model.request.CourseRequest;
import za.co.giraffe.lms_api.model.request.PublishRequest;
import za.co.giraffe.lms_api.service.course.ICourseService;

/**
 * @author Suraj Tekchandani
 */
@RestController
@RequestMapping("/admin/course")
@Api(value = "Admin API", tags = {"Admin API"})
public class CourseApiController {

    private ICourseService iCourseService;

    @Autowired
    public CourseApiController(ICourseService iCourseService) {
        this.iCourseService = iCourseService;
    }


    @ApiOperation(value = "Create Lms Course", nickname = "createLmsCourse", notes = "", response = CourseResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "successful operation", response = CourseResponse.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @PostMapping
    public ResponseEntity createLmsCourse(@RequestBody CourseRequest courseRequest) {

        return new ResponseEntity<>(iCourseService.saveCourse(courseRequest), HttpStatus.OK);
    }


    @ApiOperation(value = "Publish or UnPublish Lms CourseRequest", nickname = "publishUnPublishCourse", notes = "", response = CategoryResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "successful operation", response = String.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @PatchMapping("/{Id}")
    public ResponseEntity publishUnPublishCourse(@ApiParam(value = "Course ID", required = true) @PathVariable("Id") Integer id,
                                                 @RequestBody PublishRequest publishRequest) {
        iCourseService.publishUnPublishCourse(id, publishRequest);
        return new ResponseEntity<>("OK", HttpStatus.NO_CONTENT);
    }


    @ApiOperation(value = "Update Lms Course", nickname = "createLmsCourse", notes = "", response = CourseResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = CourseResponse.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @PutMapping("/{Id}")
    public ResponseEntity updateLmsCourse(@ApiParam(value = "Course ID", required = true) @PathVariable("Id") Integer id,
                                          @RequestBody CourseRequest courseRequest) {
        return new ResponseEntity<>(iCourseService.updateCourse(courseRequest, id), HttpStatus.OK);
    }

    @ApiOperation(value = "Get Course By Id", nickname = "publishUnPublishCourse", notes = "", response = CourseResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = CourseResponse.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @GetMapping("/{Id}")
    public ResponseEntity getCourseById(@ApiParam(value = "Course ID", required = true) @PathVariable("Id") Integer id) {
        return new ResponseEntity<>(iCourseService.getCourseById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Fetch Lms Course", nickname = "getLmsCourse", notes = "", response = CourseResponse.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = CourseResponse.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @GetMapping
    public ResponseEntity getLmsCourse() {
        return new ResponseEntity<>(iCourseService.fetchCourses(), HttpStatus.OK);
    }
}
