
/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.api.jobseeker;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.giraffe.lms_api.model.lms.CategoryResponse;
import za.co.giraffe.lms_api.model.lms.CourseOverview;
import za.co.giraffe.lms_api.model.lms.CourseResponse;
import za.co.giraffe.lms_api.model.request.CourseReviewRequest;
import za.co.giraffe.lms_api.service.category.ICategoryService;
import za.co.giraffe.lms_api.service.course.ICourseService;

import javax.validation.Valid;

/**
 * @author Suraj Tekchandani
 */
@RestController
@RequestMapping("/public")
@Api(value = "Public API", tags = {"Public API"})
public class PublicApiController {

    private ICategoryService iCategoryService;
    private ICourseService iCourseService;

    @Autowired
    public PublicApiController(ICategoryService iCategoryService,
                               ICourseService iCourseService) {
        this.iCategoryService = iCategoryService;
        this.iCourseService = iCourseService;
    }

    @ApiOperation(value = "Fetch Lms Category For Jobseeker", nickname = "getLmsCategoryForJobseeker", notes = "", response = CategoryResponse.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = CategoryResponse.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @GetMapping("/category")
    public ResponseEntity getLmsCategoryForJobseeker() {
        try {
            return new ResponseEntity<>(iCategoryService.getCategoriesForJobseeker(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @ApiOperation(value = "Fetch Lms Course For Jobseeker (Browse Course)", nickname = "getLmsCategoryForJobseeker", notes = "",
            response = CourseOverview.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = CourseOverview.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @GetMapping("/course")
    public ResponseEntity getLmsCourseForJobseeker(@ApiParam(value = "") @RequestParam(value = "category_id", required = false) Integer categoryId) {
        try {
            return new ResponseEntity<>(iCourseService.browseCourse(categoryId), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Get Lms Course For By id", nickname = "getLmsCourseByIdForJobseeker", notes = "", response = CourseOverview.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = CourseOverview.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @GetMapping("/course/{id}")
    public ResponseEntity getLmsCourseByIdForJobseeker(@ApiParam(value = "Course ID", required = true) @PathVariable("id") Integer id) {
        try {
            CourseOverview courseOverview = iCourseService.browseCourseById(id);
            if (courseOverview == null) {
                return new ResponseEntity<>("Course not found", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(courseOverview, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Review Course", nickname = "reviewCourse", notes = "", response = CourseResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = CourseResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = String.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @PostMapping("/course/{id}/action/review")
    public ResponseEntity reviewCourse(@ApiParam(value = "Course ID", required = true) @PathVariable("id") Integer id,
                                       @Valid @RequestBody CourseReviewRequest courseReviewRequest) {


        iCourseService.reviewCourse(id, courseReviewRequest);
        return new ResponseEntity<>("", HttpStatus.OK);

    }

    @ApiOperation(value = "Health check api", nickname = "healthCheck", notes = "", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/health-check",
            method = RequestMethod.GET)
    public ResponseEntity healthCheck() {
        return new ResponseEntity<>("UP", HttpStatus.OK);
    }
}
