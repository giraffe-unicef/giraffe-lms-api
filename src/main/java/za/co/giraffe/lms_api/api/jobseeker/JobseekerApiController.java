
/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.api.jobseeker;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.giraffe.lms_api.model.jobseeker.CourseStatusResponse;
import za.co.giraffe.lms_api.model.jobseeker.EnrollCourseResponse;
import za.co.giraffe.lms_api.model.jobseeker.JobseekerEnrollmentStatus;
import za.co.giraffe.lms_api.model.request.UpdateCourseStatusRequest;
import za.co.giraffe.lms_api.service.course.ICourseService;
import za.co.giraffe.lms_api.service.jobseeker.IJobseekerCourseService;

/**
 * @author Suraj Tekchandani
 */
@RestController
@RequestMapping("/jobseekers")
@CrossOrigin("*")
@Api(value = "JobSeeker API", tags = {"JobSeeker API"})
public class JobseekerApiController {

    private IJobseekerCourseService iJobseekerCourseService;
    private ICourseService iCourseService;

    @Autowired
    public JobseekerApiController(IJobseekerCourseService iJobseekerCourseService,
                                  ICourseService iCourseService) {
        this.iJobseekerCourseService = iJobseekerCourseService;
        this.iCourseService = iCourseService;

    }

    @ApiOperation(value = "Enroll Course", nickname = "enrollCourse", notes = "", response = EnrollCourseResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = EnrollCourseResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = String.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @PostMapping("/{jobseeker_id}/course/{course_id}/action/enroll")
    public ResponseEntity enrollCourse(@ApiParam(value = "Jobseeker's ID", required = true) @PathVariable("jobseeker_id") Integer jobseekerId,
                                       @ApiParam(value = "Course's ID", required = true) @PathVariable("course_id") Integer courseId) {
        return new ResponseEntity<>(iJobseekerCourseService.enrollCourse(courseId, jobseekerId), HttpStatus.OK);
    }

    @ApiOperation(value = "Update Chapter Status", nickname = "upsertCourseChapterStatus", notes = "", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = String.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @PatchMapping("/{jobseeker_id}/enrollment/{enrollment_id}/action/update-chapter-status")
    public ResponseEntity upsertCourseChapterStatus(@ApiParam(value = "Jobseeker's ID", required = true) @PathVariable("jobseeker_id") Integer jobseekerId,
                                                    @ApiParam(value = "Enrollment's ID", required = true) @PathVariable("enrollment_id") Integer enrollmentId,
                                                    @RequestBody UpdateCourseStatusRequest updateCourseStatusRequest) {

        iJobseekerCourseService.updateCourseStatus(enrollmentId, updateCourseStatusRequest);
        return new ResponseEntity<>(HttpStatus.OK);

    }


    /*@ApiOperation(value = "Review Course", nickname = "reviewCourse", notes = "", response = CourseResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = CourseResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = String.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @PostMapping("/{jobseeker_id}/enrollment/{enrollment_id}/action/review")
    public ResponseEntity reviewCourse(@ApiParam(value = "Jobseeker's ID", required = true) @PathVariable("jobseeker_id") Integer jobseekerId,
                                       @ApiParam(value = "Enrollment's ID", required = true) @PathVariable("enrollment_id") Integer enrollmentId,
                                       @RequestBody CourseReviewRequest courseReviewRequest) {


        iCourseService.reviewCourse(enrollmentId, courseReviewRequest);
        return new ResponseEntity<>("", HttpStatus.OK);

    }*/

    @ApiOperation(value = "Get Enrolled Course (Your Course)", nickname = "getEnrolledCourse", notes = "", response = EnrollCourseResponse.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = EnrollCourseResponse.class, responseContainer = "List"),
            @ApiResponse(code = 401, message = "Unauthorized", response = String.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @GetMapping("/{jobseeker_id}/course")
    public ResponseEntity getEnrolledCourse(@ApiParam(value = "Jobseeker's ID", required = true) @PathVariable("jobseeker_id") Integer jobseekerId) {
        return new ResponseEntity<>(iJobseekerCourseService.getEnrolledCourseList(jobseekerId), HttpStatus.OK);

    }

    @ApiOperation(value = "Fetch Chapter Status", nickname = "fetchCourseChapterStatus", notes = "", response = CourseStatusResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = CourseStatusResponse.class)})
    @GetMapping("/{jobseeker_id}/enrollment/{enrollment_id}/status")
    public ResponseEntity fetchCourseChapterStatus(@ApiParam(value = "Jobseeker's ID", required = true) @PathVariable("jobseeker_id") Integer jobseekerId,
                                                   @ApiParam(value = "Enrollment's ID", required = true) @PathVariable("enrollment_id") Integer enrollmentId) {

        return new ResponseEntity<>(iJobseekerCourseService.getEnrollmentStatus(enrollmentId), HttpStatus.OK);

    }

    @ApiOperation(value = "Check if jobseeker has already enrolled this course ", nickname = "getEnrolledCourse",
            notes = "", response = JobseekerEnrollmentStatus.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = JobseekerEnrollmentStatus.class, responseContainer = "List"),
            @ApiResponse(code = 401, message = "Unauthorized", response = String.class),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)
    })
    @GetMapping("/{jobseeker_id}/course/{course_id}")
    public ResponseEntity checkIfJobseekerHasEnrolledThisCourse(@ApiParam(value = "Jobseeker's ID", required = true) @PathVariable("jobseeker_id") Integer jobseekerId,
                                                                @ApiParam(value = "Course's ID", required = true) @PathVariable("course_id") Integer courseId) {
        return new ResponseEntity<>(iJobseekerCourseService.checkIfJobseekerHasEnrolledCourse(jobseekerId, courseId), HttpStatus.OK);

    }
}
