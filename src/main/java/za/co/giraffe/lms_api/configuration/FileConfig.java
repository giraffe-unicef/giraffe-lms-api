
/*
 * Copyright 2021 Giraffe
 *
 * This file is part of Giraffe opensource lms
 *
 * Giraffe opensource lms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Giraffe opensource lms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Trove Examples. If not, see <http://www.gnu.org/licenses/>.
 */
package za.co.giraffe.lms_api.configuration;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Suraj Tekchandani
 */
@Getter
@Setter
@NoArgsConstructor
@ConfigurationProperties(prefix = "lms")
public class FileConfig {
    private String file_path = "D:\\work\\giraffe\\documents\\data\\lms";
    private String file_url = "http://localhost:8081/file/";
    private String base_url = "http://localhost:8081/file/";
}
